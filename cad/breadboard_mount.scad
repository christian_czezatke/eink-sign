/*
 * Template for a simple breadboard mount.
 *
 * Creates a 3D printable mount for a breadboard with four mounting
 * holes arranged in a rectangle (see eink_mount.png).
 *
 * Parameters to specify:
 *
 * hole_distance_x, hole_distance_y --> Width and height of the rectangle
 *    that is formed by the mounting holes in the breadboard (center-hole
 *    to center-hole).
 *
 * hole_diameter --> Diameter of a mounting hold.
 *
 * board_thickness --> Thickness of the breadboard.
 *
 * clearance_height --> Distance from the top of the mounting surface to
 *    the bottom of the breadboard.-- Note that clearance is actually 2mm
 *    less in some places, due to the thickness of the mounting bracket
 *    itself.
 *
 * All measurements in millimeters (or you can scale it when you slice).
 *
 * Example:
 *
 * hole_distance_x = 58;
 * hole_distance_y = 30.2-3.7-3.5;
 * hole_diameter = 3;
 * board_thickness = 3;
 * clearance_height = 5;
 */


connector_width = 10;
connector_height = 2;
support_pad = 3; // Breadboard rests on supports with 3mm more dia than hole dia.

module arm()
{

   dist = sqrt(hole_distance_x*hole_distance_x/4+
               hole_distance_y*hole_distance_y/4);

   translate([0, dist, 0]) {
      cylinder(d=hole_diameter+support_pad, h=clearance_height);
      cylinder(d=hole_diameter, h=2*clearance_height+board_thickness);
   }

   translate([-connector_width/2,0,0])
      cube([connector_width, dist, connector_height]);

   translate([0, dist,0])
      cylinder(d=10, h=2);

   cylinder(d=connector_width,h=connector_height);
}


module breadboard_mount()
{
   phi = atan2(hole_distance_x/2, hole_distance_y/2);
   rotate([0,0,-phi])
      arm();
   rotate([0,0,phi])
      arm();
   

   mirror([0, -1, 0]) {
      rotate([0,0,-phi])
         arm();
      rotate([0,0,phi])
         arm();
   }
}
