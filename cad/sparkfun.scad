/*
 * Breadboard mount for the Sparkfun Thing 8266 board.
 *
 * Dimensions in their drawing are in inches...
 */

include <breadboard_mount.scad>

TO_MM=25.4;

hole_distance_x = 0.8 * TO_MM;
hole_distance_y = 1.25 * TO_MM;
hole_diameter = 3;

board_thickness = 3;
clearance_height = 5;

breadboard_mount();

