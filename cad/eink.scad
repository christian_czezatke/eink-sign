/*
 * Breadboard mount for the eInk shield.
 */

include  <breadboard_mount.scad>

hole_distance_x = 65-7;
hole_distance_y = 30.2-3.7-3.5;
hole_diameter = 3;
board_thickness = 3;
clearance_height = 5;

breadboard_mount();
