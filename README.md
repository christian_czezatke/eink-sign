# eInk Sign

This project realizes a "smart picture" consisting of a 7.5" eInk display driven by an ESP8266 module:

![Photo](docs/source/smart_picture.jpg)

Since the processing resources available on an ESP8266 are fairly limited, and we are running on battery, the "smart picture" itself just runs some straightforward code that periodically requests an image from an HTTP server, updates the eInk display and then goes to sleep for an amount of time specified by the HTTP reply from the server as well.

Information displayed:

* Current Date/Time.
* Air pollution from my own Purple Air sensor.
* Arrival time for the next bus (inbound/outbound) for a bus stop that is close by.
* Current amount of electricity consumed by our household vs. electricity produced by the solar panels on our building.
* Current weather and weather forecast for the next 12 hours.
* Traffic congestion numbers computed for a few points of interest within the city of San Francisco.

The project consists of several parts:

* einkdraw is a Python module for off-host rendering for images on low resolution/low color depth displays. It utilizes pycairo (binding for GTK's rendering engine) internally and then converts the resulting image to a bitmap suitable for an eInk display. It also provides some layout abstractions such as rows, columns, stacks or window nesting, that save you from most of the pixel-counting. More information about einkdraw, including API documentation is available [here](https://www.ceci.at/projects/einkdraw/)

* A server that utilizes einkdraw and the mymuni and purpleair modules from my other repositories [here](https://bitbucket.org/christian_czezatke/muni-led-sign/src/master/) and [here](https://bitbucket.org/christian_czezatke/purple-air/src/master/) to query information from various sources, process it and render an image for the "smart picture".

* Arduino code that runs on an ESP8266 board that controls the eInk display and periodically queries images from the server.

* Finally, some quick and dirty OpenSCAD code to produce 3D printable mounting brackets for breadboards, so that you can stick them to the back of the "smart picture".

Stay tuned for more information on the project to come...

## Credits

### einkdraw Bitmap Fonts

The bitmap fonts used by einkdraw are from Adafruit's GFX Library, which can be found [here](https://github.com/adafruit/Adafruit-GFX-Library).

### Icons for Weather Forecast

The icons were originally released by Design Instruct (designinstruct.com). They were obtained from endrei7c4 on github from his [8266 weather display](https://github.com/andrei7c4/weatherdisplay).
