#!/usr/bin/python3
"""
Script to generate the "Starting..." image to display when picture
and server are synchronizing.
"""
import cairo
from einkdraw.layout import Stack, Background, Column,Spacer
from einkdraw.screen import Screen
from einkdraw.glyphs import Label
from einkdraw.constants import SANS_BOLD_FACE
import sys

text_height=64

screen = Screen.Waveshare75Mono()
w = screen.width
h = screen.height
bg = Background(width=w, height=h, pattern=Background.PATTERN_LIGHT_GREY);
msg = Label(width=w, height=text_height, face=SANS_BOLD_FACE,
            text="Starting...")

spacer_height = (h-text_height)//2

fg = Column((Spacer(spacer_height), msg))

st = Stack((bg, fg))

screen.set_content(st)
screen.draw()

open(sys.argv[1], "wb").write(screen.get_data())
