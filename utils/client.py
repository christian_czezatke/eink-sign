#!/usr/bin/python3
"""

A quick and dirty python script that emulates the behavior of the smart picture frame,
for testing and development.

Can also be used to display an image rendered for the eink display by einkdraw.

"""

import requests
import cairo
import io
from gi.repository import Gtk
import time
import sys
import random

scale_factor = 3

image_url="http://127.0.0.1:5000/get_image_timed"


SLEEP_TIME=0
LATEST_REFRESH=0
DATA=None

file_name=None


def setup():
    img  = Gtk.Window(title="Test")
    img.set_default_size(640*scale_factor, 384*scale_factor)
    drawingarea = Gtk.DrawingArea()
    img.add(drawingarea)
    drawingarea.connect("draw", refresh)
    img.show_all()
    return img


def refresh(da, ctx):
    global SLEEP_TIME
    global DATA
    global LATEST_REFRESH

    print("Redrawing")

    if file_name is None:
        now = time.time()
        if DATA is None or now - LATEST_REFRESH > SLEEP_TIME:
            battery_voltage = random.randint(3600, 4200)
            params = { "client_id": "5c:cf:7f:5b:c8:7e", "voltage": battery_voltage, "type": "lipo" }    
            request = requests.get(image_url, params=params)
            raw_data = bytearray(request.content)
            DATA = raw_data
            LATEST_REFRESH = now
        else:
            raw_data = DATA
    else:
        with open(file_name, "rb") as f:
            raw_data = bytearray(f.read())

    SLEEP_TIME = int.from_bytes(raw_data[:4], "big")
    raw_data = raw_data[4:]

    new_img = cairo.ImageSurface(cairo.FORMAT_RGB24, 640, 384)
    mv = new_img.get_data()

    in_len = len(raw_data)
    cnt = 0
    num_pixels = len(raw_data) * 8

    while cnt < num_pixels:
        in_byte = raw_data[cnt//8]
        for i in range(8):
            if (in_byte & (1<<(7-i))) != 0:
                val = 0
            else:
                val = 255
                
            # Set pixel to white
            mv[4*cnt] = val
            mv[4*cnt+1] = val
            mv[4*cnt+2] = val
            mv[4*cnt+3] = 0

            cnt += 1
    mv.release()

    ctx.scale(scale_factor, scale_factor)
    ctx.set_source_surface(new_img)
    ctx.paint()
    del ctx


if len(sys.argv) == 2:
    print("Reading from file!")
    file_name = sys.argv[1]
    

img = setup()

while True:
    while Gtk.events_pending():
        Gtk.main_iteration()
    t = SLEEP_TIME
    while t > 0:
        sys.stdout.write("Sleeping for {0} seconds ".format(t))
        sys.stdout.flush()
        t -= 1
        time.sleep(1)
        sys.stdout.write("\r")
    sys.stdout.write("\n")
    sys.stdout.flush()
    img.queue_draw()

            
#ctx = cairo.Context(img)
Gtk.main()
#time.sleep(5)
