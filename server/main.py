#!/usr/bin/python3
import dashboard
from einkdraw.screen import Screen
from einkdraw.logger import set_logger, error, warn, info, debug
from flask import Flask, send_file, request
import time
import threading
from datetime import datetime, timedelta
import updaters
import sys
import os
import argparse
import logging
import logging.handlers
import json


startup_img = "startup.img"

package_directory = os.path.dirname(os.path.abspath(__file__))
startup_img_path = os.path.join(package_directory, "data", startup_img)

app = Flask("__picture_server__")
client_data = None


def update_dashboard(percentage, file_name):
    global dash
    global updater

    updater.update(5)
    dash.set_value_battery(percentage)
    return dash.output(file_name)


def get_battery_percent(b_type, volts):
    batts = {
        "lipo": [
            [3950, 75],
            [3800, 50],
            [3720, 25],
            [3600, 0]
        ]
    }

    try:
        tbl = batts[b_type]
    except KeyError:
        warn("Unkown battery type {0}.".format(b_type))
        return 0

    result = 100
    for x in tbl:
        if volts > x[0]:
            return result
        result = x[1]
    return 0


class ClientInfo(object):
    def __init__(self, client_id, force_async, config):
        air = config["air_quality"]
        bus = config["bus"]
        energy = config["energy"]
        weather = config["weather"]
        traffic = config["traffic"]

        clients = config["general"]["clients"]
        if client_id in clients.keys():
            self.voltage_correction_factor = float(clients[client_id]["voltage_correction_factor"])
        else:
            info("No voltage correction factor for client {0} in configuration.".format(client_id))
            self.voltage_correction_factor = 1


        traffic_bbox = [float(x) for x in traffic["bounding_box"]]
        traffic_poi = dict()
        for i in traffic["poi"]:
            k = i["name"]
            v = i["location"]
            traffic_poi[k] = [float(x) for x in v]

        traffic_order = [x["name"] for x in traffic["poi"]]

        self.client_id = client_id
        self.force_async = force_async
        self.update_timeout = config["general"]["update_timeout"]/1000
        self.config = config
        self.dash = dashboard.Dashboard(Screen.Waveshare75Mono(),
                                        bus_label=bus["name"],
                                        traffic_locations=traffic_order)
        self.updater = \
        updaters.UpdateContainer(((updaters.AQIUpdater(air["sensor_name"]), self.dash.set_value_aqi),
                                  (updaters.MuniUpdater(bus["line"], bus["inbound"],
                                                        bus["outbound"]),
                                   self.dash.set_value_bus_prediction),
                                  (updaters.EnergyUpdater(energy["url"]), self.dash.set_value_solar),
                                  (updaters.WeatherUpdater(weather["api_key"],
                                                           weather["location"],
                                                           weather["temperature_unit"]),
                                   self.dash.set_value_weather),
                                  (updaters.TrafficUpdater(traffic["api_key"],
                                                           traffic_bbox,
                                                           traffic_poi,
                                                           traffic["radius"]/1000),
                                   self.dash.set_value_traffic)))
        self.thread_dict = dict()
        self.cur_thread_id = 0
        self.lock = threading.Lock()
        self.battery_type = None
        self.battery_voltage = 0
        self.timestamp = 0
        self.update_duration = 0
        self.data = None


    def update(self, delay, update_id):
        time.sleep(delay)
        with self.lock:
            try:
                canceled = self.thread_dict[update_id]
                del self.thread_dict[update_id]
            except KeyError:
                return

            if not canceled:
                self._update()


    def _update(self):
        start = time.time()
        percentage = get_battery_percent(self.battery_type, self.battery_voltage)
        self.updater.update(self.update_timeout)
        self.dash.set_value_battery(percentage)
        self.data = self.dash.output()
        debug("Output created.")
        self.timestamp = time.time()
        self.update_duration = self.timestamp - start
        info("Update for client {0} took {1}s.".format(self.client_id, self.update_duration))


    def get_data(self, battery_type, battery_voltage):

        corrected_voltage = battery_voltage * self.voltage_correction_factor
        info("VOLTAGE: Client {0} {1} --> {2}".format(self.client_id, battery_voltage,
                                                      corrected_voltage))

        with self.lock:
            self.battery_voltage = corrected_voltage
            self.battery_type = battery_type

            delta = time.time() - self.timestamp
            if  delta > 15 and self.data is not None:
                debug("Discarding outdated info ({0}s old).".format(int(delta)))
                self.data = None

            if self.data is None:
                if self.force_async:
                    try:
                        result = open(startup_img_path, "rb").read()
                    except Exception as e:
                        error("Cannot load startup image: {}".format(e))
                        result = bytearray([0]*(640*384//8))
                else:
                    self._update()
                    result = self.data
            else:
                result = self.data
                self.data = None
            return result


    def client_complete(self):
        """
        client phoned in that the request is complete.
        """

        now = int(time.time())
        next_minute = 60 - (now % 60) + 1
        preload_time = next_minute - self.update_duration - 2
        client_wait_time =  next_minute

        with self.lock:
            self._schedule_new_update(preload_time)
        return client_wait_time


    def _cancel_current_update(self):
        try:
            self.thread_dict[self.cur_thread_id] = True
        except KeyError:
            pass


    def _schedule_new_update(self, delay):
        self._cancel_current_update()
        self.cur_thread_id += 1
        self.thread_dict[self.cur_thread_id] = False
        thread = threading.Thread(target=self.update, args=[delay, self.cur_thread_id])
        thread.start()


class RequestInfo(object):
    def __init__(self, force_async, config):
        self.clients = dict()
        self.force_async = force_async
        self.config = config

    def get_client(self, client_id):
        if client_id not in self.clients.keys():
            self.clients[client_id] = ClientInfo(client_id, self.force_async,
                                                 self.config)
        return self.clients[client_id]


@app.route("/get_image_timed")
def get_image():
    global client_data

    start = time.time();
    info("get_image_timed request received.")

    battery_type = request.args.get("type")
    battery_voltage = int(request.args.get("voltage"))
    client_id = request.args.get("client_id")

    client = client_data.get_client(client_id)
    data = client.get_data(battery_type, battery_voltage)
    wait_time = client.client_complete()

    info("get_image took {0}s, wait time: {1}s.".format(time.time()-start, wait_time))
    return wait_time.to_bytes(4, byteorder="big") + data


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--force-async",
                        help="Always compute updates asynchronously (recommended for slow servers).",
                        dest="force_async",
                        default=False, action="store_true")
    parser.add_argument("--logfile",
                        help="Logfile name and location",
                        dest="logfile",
                        type=str, default="einkserver.log")
    parser.add_argument("--cfgfile",
                        help="Path to configuration file",
                        dest="cfgfile",
                        type=str, default="einkserver.cfg")
    return parser.parse_args()


def get_logger(file_name):
    root = logging.getLogger()
    h = logging.handlers.RotatingFileHandler(file_name, 'a', maxBytes=4000000, backupCount=10)
    f = logging.Formatter('%(asctime)s %(processName)-10s %(name)s %(levelname)-8s %(message)s')
    h.setFormatter(f)
    root.addHandler(h)
    return root


def run():
    global client_data

    args = get_arguments()
    logger = get_logger(args.logfile)
    logger.setLevel(logging.INFO)
    set_logger(logger)

    info("Reading config file {0}".format(args.cfgfile))

    try:
        with open(args.cfgfile, "r") as f:
            config = json.loads(f.read())
    except Exception as e:
        error("Unable to read configuration file: {0}".format(e))
        sys.exit(1)

    client_data = RequestInfo(args.force_async, config)

    info("Pre-loading clients")

    for client_id, cfg in config["general"]["clients"].items():
        if cfg["preload"]:
            client_data.get_client(client_id)

    info("starting server.")
    app.run(host="0.0.0.0")




def test_dashboard():
    dash = dashboard.Dashboard(Screen.Waveshare75Mono())

    # Loop to poll and set values goes here.

    dash.set_value_solar(1.5, 0.8)
    dash.set_value_bus_prediction(None, 12)
    dash.set_value_aqi(("Upper Noe Valley", 152, 151, 151, 150, 147, 105, 45))
    dash.set_value_weather((
        ( "clear", 72, 7, 15, 77, 62),
        (
            ("2pm", "overcast", 63, 20, 5),
            ("5pm", "partly_cloudy", 57, 20, 5),
            ("8pm", "drizzle", 53, 50, 5),
            ("11pm", "heavy_rain", 55, 100, 5)
        )))


    dash.output("first.png")


if __name__ == "__main__":
    run()
