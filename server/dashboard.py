import os
import cairo
import time
from einkdraw.constants import DEFAULT_FACE, DEFAULT_BOLD_FACE, SANS_FACE, SANS_BOLD_FACE
from einkdraw.layout import Drawable, drawing_on, Wrapper, Spacer, Frame, FixedColumn, FixedRow, \
                            Column, Row, Stack, Background
from einkdraw.glyphs import Icon, Label
from einkdraw.uielements import Window, Calendar, Clock, Gauge, Barchart,BatteryIndicator
from einkdraw.screen import Screen
import einkdraw.fonts as fonts
from einkdraw.logger import error, warn, info, debug
from purpleair.aqi import aqi_to_label

package_directory = os.path.dirname(os.path.abspath(__file__))


def degree(x):
    s = "{}" + chr(176)
    return s.format(x)



class EinkDayTime(Wrapper):
    def __init__(self, unit_size=160, time_offset=0):
        fm = fonts.FontManager.get()
        font = fm.find_font("Sans", bold=True, max_size=24)
        
        self.calendar = Calendar(width=unit_size, height=unit_size,
                                 day_face=SANS_FACE,
                                 minor_face=font,
                                 minor_background=Background.PATTERN_LIGHT_GREY,
                                 offset=time_offset)
        self.clock = Clock(width=unit_size, background=Background.PATTERN_LIGHT_GREY,
                           offset=time_offset)
        row = Column((self.clock, Spacer(5), self.calendar))
        super().__init__(Frame(row, 2, 1, 2))


class EinkElectricity(Window):
    def __init__(self, unit_size=160, height=160):
        self.consumption = \
            Gauge(width=unit_size, gauge_thickness=20, chart_type="semicircular",
                  min_val=0, max_val=3,
                  value_format="2.1f", face=SANS_FACE, units="kW",
                  background=Background.PATTERN_WHITE)

        fm = fonts.FontManager()
        font = fm.find_font("Sans", max_size=19)
        
        label = Label("Consumption", width=unit_size, height=14,
                      face=font)
        c1 = Column((self.consumption, Spacer(4), label))

        self.production = \
            Gauge(width=unit_size, gauge_thickness=20, chart_type="semicircular",
                  min_val=0, max_val=3,
                  value_format="2.1f", face=SANS_FACE, units="kW",
                  background=Background.PATTERN_WHITE,
                  foreground=Background.PATTERN_MEDIUM_GREY)
        label = Label("Production", width=unit_size, height=14, face=font)
        c2 = Column((self.production, Spacer(4), label))

        gauge_row = Row((Spacer(3), c1, Spacer(1), c2, Spacer(1)))
        w, h = gauge_row.get_bbox()

        column = Column((Spacer(2),gauge_row))
        super().__init__("Home Energy Flow", column)

    def set_value(self, consumption, production):
        self.consumption.set_value(consumption)
        self.production.set_value(production)


class EinkBusDirection(Wrapper):
    def __init__(self, width, height, direction):
        self.prediction = Label(text="12", descenders=False, face=SANS_FACE)

        fm = fonts.FontManager.get()
        font = fm.find_font("Sans", max_size=16, descend=False, bold=True)
        
        c = FixedColumn(width, height,
                        ((1, Spacer(), Background.PATTERN_TRANSPARENT),
                         (5, Label(text=direction, face=font),
                          Background.PATTERN_TRANSPARENT),
                         (14, self.prediction, Background.PATTERN_TRANSPARENT),
                         (1, Spacer(), Background.PATTERN_TRANSPARENT),
                         (4, Label(text="minutes", face=font), Background.PATTERN_TRANSPARENT),
                        ))
        super().__init__(c)

    def set_value(self, val):
        if val is None:
            val="-?-"
        self.prediction.set_value("{}".format(val))



class EinkBusPrediction(Window):
    def __init__(self, unit_size, label):
        self.inbound = EinkBusDirection(unit_size+2, unit_size, direction="Inbound")
        self.outbound = EinkBusDirection(unit_size+2, unit_size, direction="Outbound")
        prediction_row = Row((self.inbound, Spacer(5), self.outbound, Spacer(5)))
        prediction_col = Column((Spacer(2), prediction_row, Spacer(2)))
        w, h = prediction_col.get_bbox()
        bg = Background(w, h, Background.PATTERN_WHITE)
        stack = Stack((bg, prediction_col))
        super().__init__(label, stack)

    def set_value(self, inbound, outbound):
        self.inbound.set_value(inbound)
        self.outbound.set_value(outbound)


class EinkAQI(Window):
    def __init__(self, height, label):

        fm = fonts.FontManager.get()
        font = fm.find_font("Sans", max_size=19)
        
        self.values = ["Unknown"] + [0 for x in range(7)]

        self.aqi_current_number = Label(text="0", descenders=False, face=SANS_FACE)
        self.aqi_current_label = Label(text="Unknown", descenders=False,
                                       face=SANS_BOLD_FACE)
        self.aqi_sensor_label = Label(text="???", face=font)

        stack = FixedColumn(119, height,
                            ((6,Spacer(), None),
                             (15, self.aqi_sensor_label, None),
                             (5,Spacer(), None),
                             (30, self.aqi_current_number, Background.PATTERN_WHITE),
                             (4, Spacer(), None),
                             (20, self.aqi_current_label, None),
                             (0,Spacer(), None)))

        self.bars = Barchart(height=height, min_val=0, max_val=300, bar_width=9,
              categories = ( \
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True),
              Background(pattern=Background.PATTERN_MEDIUM_GREY, draw_outline=True))
              )

        bg = Background(*self.bars.get_bbox(), pattern=Background.PATTERN_LIGHT_GREY)
        bar_stack = Stack((bg, self.bars))

        content = Column((Spacer(1),
                          Row((stack, Spacer(5), bar_stack, Spacer(3))),
                          Spacer(3)))
        super().__init__("Air Quality", content)


    def set_value(self, values):
        #assert len(values) == len(self.values)            
        self.values = values

        label = values[0]
        # XXX: Hack -- Cut long sensor label
        if len(label) > 15:
            label = label[:9] + "..."
        
        self.aqi_sensor_label.set_value(label)
        self.aqi_current_number.set_value("{}".format(self.values[1]))
        self.aqi_current_label.set_value(aqi_to_label(self.values[1]))
        self.bars.set_value(values[1:])

class EinkMessages(Window):
    def __init__(self, label, width, height, num_lines=3):
        self.width = width
        self.height = height
        self.lines = [Label(text="", alignment="left", clip_text=False,
                            face=DEFAULT_BOLD_FACE) for x in range(num_lines)]
        
        stack = FixedColumn(width, height, [(1, x, None) for x in self.lines])
        super().__init__(label, stack)

    def set_value(self, values):
        assert len(values) == len(self.lines)
        for label, new_val in zip(self.lines, values):
            label.set_value(new_val)


class EinkTraffic(Window):
    def __init__(self, label, width, height, locations=[]):
        self.width = width
        self.height = height

        fm = fonts.FontManager()
        font = fm.find_font("Sans", max_size=19)
        
        self.values = dict()
        elements = []
        for l in locations:
            
            self.values[l] = Label(text="", alignment="centered", descenders=False,
                                   face=SANS_FACE)
            name = Label(text=l, alignment="centered",
                         face=font)
            stack = FixedColumn(width/len(locations), height,
                                ((5, Spacer(), None),
                                 (40, self.values[l], None),
                                 (5, Spacer(), None),
                                 (10, name, None)))
            elements.append(stack)
        
        row = Row(elements)
        super().__init__(label, row)

    def set_value(self, kv):
        assert len(kv.keys()) == len(self.values.keys())
        for k, v in kv.items():
            self.values[k].set_value(str(v))
            
            
class HourlyForecast(Wrapper): #12 higher
    def __init__(self, width):
        fm = fonts.FontManager.get()
        font = fm.find_font("Sans", bold=True, max_size=19)
        
        self.timestamp = Label(text="2pm", height=18, width=width, face=font)
        self.icon = Icon(64, 64)
        self.temp = Label(text="32F", face=font)
        self.precip = Label(text="0%", face=font)
        self.wind = Label(text="12m/h", face=font)

        icon_row = Row((Spacer((width-64)/2), self.icon, Spacer((width-64)/2)))

        stack = FixedColumn(width, 50,
                            ((1, self.temp, None),
                             (1, self.precip, None),
                             (1, self.wind, None)))
        column = Column((self.timestamp, icon_row, Spacer(2), stack))
        super().__init__(column)

    def set_value(self, value):
        self.timestamp.set_value(value[0])
        self.icon.set_value(value[1])

        val = "-?-" if value[2] is None else round(value[2])
        self.temp.set_value(degree(val))

        val = "-?-" if value[3] is None else round(value[3])
        self.wind.set_value("{}m/h".format(val))

        val = "-?-" if value[4] is None else "{0:1.2f}".format(value[4])
        self.precip.set_value("{}\"".format(val))


class CurrentWeather(Wrapper):
    def __init__(self, width, height):
        self.icon = Icon(64, 64)
        self.current_temp = Label(text="-?-", height=0.55*height, width=1.2*height,
                                  face=SANS_FACE,
                                  descenders=False, alignment="centered")
        self.current_precip = Label(width=120, height=24, text="-?-", descenders=False, alignment="right",
                                    face=SANS_FACE)
        self.current_wind = Label(width=120, height=24, text="-?-", descenders=False, alignment="left",
                                  face=SANS_FACE)
        self.high = Label(text="-?-", descenders=False, alignment="right",
                          text_color=Background.PATTERN_MEDIUM_GREY,
                          face=SANS_FACE)
        self.low = Label(text="-?-", descenders=False, alignment="right",
                         text_color=Background.PATTERN_MEDIUM_GREY,
                         face=SANS_FACE)


        high_low = FixedColumn(80, 0.55*height,
                               ((10, self.high, None),
                                (1, Spacer(), None),
                                (10, self.low, None)
                               ))

        row_1 = Row((Spacer(3), self.icon, Spacer(16), self.current_temp, high_low))
        row_2 = Row((Spacer(10), self.current_precip, Spacer(20), self.current_wind))

        current = Column((Spacer(3), row_1, row_2, Spacer(3)))
        super().__init__(current)


    def set_value(self, value):
        val = None if value[0] is None else value[0]
        self.icon.set_value(val)

        val = "-?-" if value[1] is None else round(value[1])
        self.current_temp.set_value(degree(val))

        val = "-?-" if value[2] is None else "{0: 3}".format(round(value[2]))
        self.current_wind.set_value("{}m/h".format(val))

        val = "-?-" if value[3] is None else "{0:1.2f}".format(value[3])
        self.current_precip.set_value("{0}\"".format(val))

        val = "-?-" if value[4] is None else round(value[4])
        self.high.set_value(degree(val))

        val = "-?-" if value[5] is None else round(value[5])
        self.low.set_value(degree(val))



class WeatherPanel(Wrapper):
    def __init__(self, width, height):
        self.width = width
        self.height = height

        load = cairo.ImageSurface.create_from_png
        self.day_icons = {
            "clear": load("icons/icon64_01d.png"),
            "overcast": load("icons/icon64_02d.png"),
            "partly_cloudy": load("icons/icon64_03d.png"),
            "cloudy": load("icons/icon64_04d.png"),
            "drizzle": load("icons/icon64_10d.png"),
            "rain": load("icons/icon64_09d.png"),
            "heavy_rain": load("icons/icon64_13d.png"),
            "fog": load("icons/icon64_50d.png"),
            "thunderstorm": load("icons/icon64_11d.png"),
        }
        self.night_icons = {
            "clear": load("icons/icon64_01n.png"),
            "overcast": load("icons/icon64_02n.png"),
            "partly_cloudy": load("icons/icon64_03d.png"),
            "cloudy": load("icons/icon64_04d.png"),
            "drizzle": load("icons/icon64_10d.png"),
            "rain": load("icons/icon64_09d.png"),
            "heay_rain": load("icons/icon64_13d.png"),
            "fog": load("icons/icon64_50n.png"),
            "thunderstorm": load("icons/icon64_11d.png"),
        }

        self.current = CurrentWeather(self.width, 100)

        self.hourlies = [HourlyForecast(width/4) for x in range(4)]

        hourly_row = Row()
        for idx in range(len(self.hourlies)):
            hourly = self.hourlies[idx]
            w, h = hourly.get_bbox()
            bg = Background(*hourly.get_bbox(),
                            Background.PATTERN_LIGHT_GREY if idx %2 == 1 else
                            Background.PATTERN_WHITE)
            hourly_row.add(Stack((bg, hourly)))

        super().__init__(Column((self.current, Spacer(3), hourly_row)))

    def set_value(self, value):
        cur_args = value[0]

        load = cairo.ImageSurface.create_from_png
        icon = self._load_icon(cur_args[0])
        args = (icon, *cur_args[1:])
        self.current.set_value(args)

        for hourly, forecast in zip(self.hourlies, value[1]):
            icon = self._load_icon(forecast[1])
            args = (forecast[0], icon, *forecast[2:])
            hourly.set_value(args)


    def _load_icon(self, icon_id):

        icon_template = os.path.join(package_directory, "icons", "icon64_{0}.png")

        if not os.path.exists(icon_template.format(icon_id)):
            # Try a day icon. When day and night icons are identical, we only have one.
            icon_id = icon_id[:-1] + "d"

            if not os.path.exists(icon_template.format(icon_id)):
                x = icon_template.format(icon_id)
                error("Weather panel. Icon not found: {0}".format(x))
                return None

        return cairo.ImageSurface.create_from_png(icon_template.format(icon_id))


class EinkWeather(Window):
    def __init__(self, width, height, label):
        self.width = width
        self.height = height
        self.label = label
        self.content = WeatherPanel(width, height)
        super().__init__(label, self.content)

    def set_value(self, value):
        self.content.set_value(value)



class Dashboard(object):

    def __init__(self, screen, bus_label, traffic_locations):
        self.screen = screen

        self.edt = EinkDayTime(unit_size=120, time_offset=20)
        self.solar = EinkElectricity(unit_size=160, height=100)
        self.bus_prediction = EinkBusPrediction(unit_size=88, label=bus_label)
        self.aqi = EinkAQI(106, "Air Quality")
        self.weather = EinkWeather(width=296, height=230, label="Weather")
        #self.messages = EinkMessages("Messages", width=296, height=100)
        self.traffic = EinkTraffic("Traffic (experimental)", width=296, height=100,
                                   locations=traffic_locations)

        weather_column = Column((Spacer(2), self.weather))
        preds = Column((Spacer(2), self.aqi, Spacer(10), self.bus_prediction))
        row = Row((self.edt, Spacer(8), preds, Spacer(10), weather_column))
        row2 = Row((Spacer(2), self.solar, Spacer(10), self.traffic))

        self.bi = BatteryIndicator()

        s = Spacer()
        s.resize(600, 1)
        
        overlay = Row((s, Column((Spacer(2),self.bi))))
        main = Column((row, Spacer(10), row2))
        
        self.screen.set_content(Stack((main, overlay)))

        debug("Dashboard weather size: {0}".format(self.weather.get_bbox()))
        debug("Dashboard clock size: {0}".format(self.edt.get_bbox()))


    def set_value_solar(self, data):
        self.solar.set_value(*data)

    def set_value_bus_prediction(self, data):
        self.bus_prediction.set_value(*data)

    def set_value_aqi(self, time_series):
        self.aqi.set_value(time_series)

    def set_value_weather(self, weather_value):
        self.weather.set_value(weather_value)

    def set_value_battery(self, battery_value):
        self.bi.set_value(battery_value)
        
    def set_value_traffic(self, data):
        self.traffic.set_value(data)

        
    def output(self, file_name=None):
        self.screen.draw()
        if file_name is not None:
            self.screen.write_to_png(file_name)

        return self.screen.get_data()


def test_dashboard():
    dash = Dashboard(Screen.Waveshare75Mono())

    # Loop to poll and set values goes here.

    dash.set_value_solar(1.5, 0.8)
    dash.set_value_bus_prediction(None, 12)
    dash.set_value_aqi((152, 151, 151, 150, 147, 105, 45))
    dash.set_value_weather((
        ( "clear", 72, 7, 15, 77, 62),
        (
            ("2pm", "overcast", 63, 20, 5),
            ("5pm", "partly_cloudy", 57, 20, 5),
            ("8pm", "drizzle", 53, 50, 5),
            ("11pm", "heavy_rain", 55, 100, 5)
        )))


    # messages.set_value(("The stock market is down today.",
    #                     "The cat has NOT been fed.",
    #                     "The car is charging."))

    dash.output("first.png")


if __name__ == "__main__":
    test_dashboard()
