import threading
import time
import json
import requests
from dateutil import tz
from geopy import distance
import pyowm
from purpleair.sensors import SensorHandle, Directory
from purpleair.aqi import pm25_to_aqi
import mymuni
import math
from einkdraw.logger import error, warn, info, debug

#Refresh weather data every 300 seconds.
WEATHER_WAIT=300

# Refresh traffic update every 300 seconds.
TRAFFIC_WAIT=300



class Updater(object):
    def __init__(self, name):
        self.thread = None
        self.data = None
        self.time_needed = 0
        self.timestamp = 0
        self.name = name

    def start_update(self):
        self.thread = threading.Thread(target=self.run)
        self.thread.start()

    def join(self, timeout):
        if timeout > 0 or not self.thread.is_alive():
            self.thread.join(timeout)
        if self.thread.is_alive():
            warn("{0} update has timed out.".format(self.name))
            return time.time(), None, self.get_default_value()

        return self.timestamp, self.time_needed, self.data

    def run(self):
        start_time = time.time()
        self.data = self._update_func()
        self.timestamp = time.time()
        self.time_needed = self.timestamp - start_time


    def get_name(self):
        return self.name

    def _update_func(self):
        return None

    def get_default_value(self):
        pass

class MuniUpdater(Updater):
    def __init__(self, route, inbound_id, outbound_id):
        self.route = route
        self.inbound = inbound_id
        self.outbound = outbound_id
        super().__init__("Muni")
        mymuni.init()


    def _update_func(self):
        result = []
        for x in (self.inbound, self.outbound):
            prediction = mymuni.get_prediction(self.route, x)
            try:
                minutes = prediction['direction'][0]['prediction'][0]['@minutes']
            except Exception as e:
                warn("MUNI ISSUE: {0} {1}".format(e, prediction))
                minutes = None
            result.append(minutes)
        return result

    def get_default_value(self):
        return (None, None)


class AQIUpdater(Updater):
    def __init__(self, sensor_name):
        self.sensor_dir = Directory()
        self.sensor_name = sensor_name
        self.my_sensor = \
            self.sensor_dir.get_sensors(filter_func=lambda x: x['CH_A']['Label'] == sensor_name)[0]
        super().__init__("AQI")

    def _update_func(self):
        aqi_sensor_data = self.my_sensor.get_live_data()
        timeline = json.loads(aqi_sensor_data["Stats"])
        aqi = [self.sensor_name]
        for x in [ "v", "v1", "v2", "v3", "v4", "v5", "v6"]:
            aqi.append(pm25_to_aqi(timeline[x]))
        return aqi

    def get_default_value(self):
        return [self.sensor_name] + [None for x in range(7)]


class EnergyUpdater(Updater):
    def __init__(self, url):
        self.url = url
        super().__init__("Energy")

    def _update_func(self):
        try:
            response = requests.get(self.url + "/api/sdata.wsgi/current")
            latest = response.json()["data"]["latest"]
            pge = latest["pge"]
            solar = latest["solar"]
            house_solar = latest["house-solar"]

            if pge < 0:
                # We are EXPORTING
                production = solar - pge
                consumption = house_solar
            else:
                # We are IMPORTING
                production = house_solar
                consumption = pge + house_solar

            return (float(consumption)/1000, float(production)/1000)
        except Exception as e:
            error("Exception in Energy Updater: {0}".format(e))
            return (None, None)

    def get_default_value(self):
        return (None, None)


class TrafficUpdater(Updater):
    TRAFFIC_URL="https://www.mapquestapi.com/traffic/v2/incidents"
    
    def __init__(self, api_key, bbox, points, radius, wait_time=TRAFFIC_WAIT):
        super().__init__("Traffic")
        self.api_key = api_key
        self.update_time = 0
        self.bbox = bbox
        self.points = points
        self.radius = radius
        self.wait_time = wait_time
        self.values = self.get_default_value()

    def _update_func(self):
        now = time.time()
        if now - self.update_time < self.wait_time:
            info("Traffic data update not yet required, waiting...")
        else:
            self.values = self._incident_report()
            self.update_time = time.time()
        return self.values


    def _summarize_report(self, report):
        result = dict()
        for x in self.points.keys():
            result[x] = 0

        for i in report["incidents"]:
            if not i["impacting"]:
                continue

            # Now check if we are within distance of one of or points of interest
            for name, location, in self.points.items():
                d = distance.distance(location, (i["lat"], i["lng"]))
                if d.km <= self.radius:
                    result[name] += i["severity"]
        return result


    def _incident_report(self):
        bbox_txt="{0},{1},{2},{3}".format(*self.bbox)
        args= {
            "outFormat": "json",
            "boundingBox": bbox_txt,
            "key": self.api_key,
            "filters": "incidents,construction,event,congestion",
        }
        report = requests.get(TrafficUpdater.TRAFFIC_URL, params=args).json()
        result = self._summarize_report(report)
        return result


    def get_default_value(self):
        result = dict()
        for x in self.points.keys():
            result[x] = "?"
        return result


class WeatherUpdater(Updater):
    def __init__(self, api_key, location, temperature_unit):
        self.api_key = api_key
        self.location = location
        self.temperature_unit = temperature_unit
        self.owm = pyowm.OWM(api_key)
        self.latest_update = 0
        self.forecast_data = None
        super().__init__("Weather")


    def _get_rain(self, w, current):
        rain = w.get_rain()
        try:
            mm_rain = rain["1h"] if current else rain["3h"]
        except KeyError:
            mm_rain = 0.0
        return mm_rain/25.4


    def _update_func(self):
        cur_time = time.time()
        delta_t = cur_time - self.latest_update
        if self.forecast_data is not None and  delta_t< WEATHER_WAIT:
            info("Weather forecast is only {0} seconds old. Returning cached value.".format(delta_t))
            return self.forecast_data
        try:
            observation = self.owm.weather_at_place(self.location)
        except pyowm.exceptions.OWMError as e:
            error("Error fetching weather: {0}".format(e))
            return self.get_default_value()

        w = observation.get_weather()
        temps = w.get_temperature(self.temperature_unit)
        now = (w.get_weather_icon_name(), temps["temp"],
               w.get_wind("miles_hour")["speed"], self._get_rain(w, True),
               temps["temp_max"], # forecast high
               temps["temp_min"]) # forecast low

        fc = self.owm.three_hours_forecast(self.location)
        forecast = fc.get_forecast()

        forecast_data = []
        for x in range(4):
            w = forecast.get(x)

            timestamp = w.get_reference_time("date")
            time_str = timestamp.astimezone(tz.tzlocal()).strftime("%l%P").strip()
            temps = w.get_temperature(self.temperature_unit)
            x = (time_str, w.get_weather_icon_name(), temps["temp"],
                 w.get_wind("miles_hour")["speed"], self._get_rain(w, False),
                 )
            forecast_data.append(x)

        self.forecast_data = (now, forecast_data)
        self.latest_update = cur_time

        return self.forecast_data


    def get_default_value(self):
        res = [[None, None, None, None, None, None]]

        for x in range(4):
            res.append([None for i in range(5)])
        return res


class UpdateContainer(object):
    def __init__(self, updaters):
        self.updaters = updaters

    def update(self, timeout):
        for updater, _ in self.updaters:
            updater.start_update()

        end_time = time.time() + timeout

        start_time = time.time()
        for updater, push_func in self.updaters:
            remain = end_time - time.time()
            timestamp, time_needed, data = updater.join(remain)
            debug("{0} updater took {1} seconds.".format(updater.get_name(), time_needed))
            push_func(data)

        info("Update took {0}s.".format(time.time() - start_time))
