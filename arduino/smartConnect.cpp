/**
 * Implementation of WiFiConnector.
 */

#include "smartConnect.h"

WiFiConnector::WiFiConnector(const char *ssid, const char *passphrase, uint32_t rtcOffset):
   ssid(ssid),
   passphrase(passphrase),
   connectingFast(false),
   rtcOfs(rtcOffset)
{
   beginDHCPSetup();
}


WiFiConnector::WiFiConnector(const char *ssid, const char *passphrase, const IPAddress &my_ip,
                             const IPAddress &my_gw, const IPAddress &my_netmask, uint32_t rtcOffset):
   ssid(ssid),
   passphrase(passphrase),
   connectingFast(false),
   rtcOfs(rtcOffset)
{
   beginSetup(my_ip, my_gw, my_netmask);
}


wl_status_t
WiFiConnector::startConnecting()
{
   wl_status_t result;

   connectingFast = true;

   result = connectFast();
   if (result == WL_CONNECT_FAILED) {
      // We can't do a fast connect. Do a slow connect.
      connectingFast = false;
      result = connectSlow();
   }

   return result;
}


wl_status_t
WiFiConnector::waitForConnection(int32_t timeout_ms)
{
   wl_status_t result;

   if (timeout_ms == 0) {
      timeout_ms = connectingFast ? FAST_CONNECT_TIMEOUT : SLOW_CONNECT_TIMEOUT;
   }

   result = waitConnected(timeout_ms);
   if (result == WL_CONNECTED) {
      Serial.println("WiFi connected");

      // If this was a slow connect, update RTC data
      if (!connectingFast) {
         saveRTC();
      }
   } else if (connectingFast) {
      Serial.println("Fast WiFi connect failed. Trying slow.");
      connectSlow();
      result = waitConnected(SLOW_CONNECT_TIMEOUT);
      if (result == WL_CONNECTED) {
         // Slow connect worked. Likely our cached WiFi info was outdated, so refresh.
         saveRTC();
      }
   }

   return result;
}

//
//
// Private member functions
//
//

void
WiFiConnector::beginDHCPSetup()
{
   /* Turn on WiFi client mode. Setting nothing else will cause fallback on DHCP. */
   WiFi.forceSleepWake();
   yield();
   WiFi.persistent(false);
   WiFi.mode(WIFI_STA);
}


void
WiFiConnector::beginSetup(const IPAddress &my_ip, const IPAddress &my_gw, const IPAddress &my_netmask)
{
   beginDHCPSetup();

   // Prevent DHCP using the config info provided.
   WiFi.config(my_ip, my_gw, my_netmask);
}


/*
 * Attempt connecting to WiFi using cached BSSID and channel info (if available).
 */

wl_status_t
WiFiConnector::connectFast()
{
   WiFi_Persistent wifiData;
   wl_status_t result;

   ESP.rtcUserMemoryRead(rtcOfs, (uint32_t*)&wifiData, sizeof(wifiData));

   if (RTC_PERSISTENT_MAGIC != wifiData.magic) {
      Serial.println("No data for fast WiFi connect");
      result = WL_CONNECT_FAILED;
   } else {
      // We do have data. Attempt a fast connect
      Serial.println("Attempting fast WiFi connect");
      result = WiFi.begin(ssid, passphrase, wifiData.channel, wifiData.bssid, true);
   }

   return result;
}


/*
 * Attempt connecting to WiFi without cached BSSID and channel number. This is slower
 * since we have to scan for the correct WiFi network to connect to.
 */

wl_status_t
WiFiConnector::connectSlow()
{
   Serial.printf("begin: %s %s\n", ssid, passphrase);
   return WiFi.begin(ssid, passphrase);
}


/*
 * Wait until our WiFi status changes to "connected" or we hit a timeout.
 */

wl_status_t
WiFiConnector::waitConnected(int32_t timeout_ms)
{
   wl_status_t result;

   result = WiFi.status();
   while (result != WL_CONNECTED && timeout_ms > 0) {
      delay(50);
      timeout_ms -= 50;
      result = WiFi.status();
   }

   return result;
}


/*
 * Save WiFi BSSID and channel number to RTC memory, so that next time we connect
 * we can use these cached values.
 */

void
WiFiConnector::saveRTC()
{
   WiFi_Persistent wifiData;

   memcpy(wifiData.bssid, WiFi.BSSID(), sizeof(wifiData.bssid));
   wifiData.channel = WiFi.channel();
   wifiData.magic = RTC_PERSISTENT_MAGIC;
   ESP.rtcUserMemoryWrite(rtcOfs, (uint32_t*)&wifiData, sizeof(wifiData));
}
