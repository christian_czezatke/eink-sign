/*
 * Embedded code to run on the smart picture frame itself.
 * Sleeps, retrieves new image from server (thereby also sending back
 * a voltage reading for the on-board battery) and displays the
 * new image, then goes back to sleep...
 *
 * Copy eink_config.sample to eink_config.h and edit to suit your needs.
 */

#include "eink_config.h"
#include "smartConnect.h"
#include <GxEPD.h>
#include <GxFont_GFX.h>
#include <GxIO/GxIO_SPI/GxIO_SPI.h>
#include <GxIO/GxIO.h>

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <Fonts/FreeMonoBold12pt7b.h>

// We use a 7.5" monochrome eInk display.
//#include <GxGDEP015OC1/GxGDEP015OC1.h>    // 1.54" b/w
//#include <GxGDEW0154Z04/GxGDEW0154Z04.h>  // 1.54" b/w/r 200x200
//#include <GxGDEW0154Z17/GxGDEW0154Z17.h>  // 1.54" b/w/r 152x152
//#include <GxGDEW0213I5F/GxGDEW0213I5F.h>  // 2.13" b/w 104x212 flexible
//#include <GxGDE0213B1/GxGDE0213B1.h>      // 2.13" b/w
//#include <GxGDEW0213Z16/GxGDEW0213Z16.h>  // 2.13" b/w/r
//#include <GxGDEH029A1/GxGDEH029A1.h>      // 2.9" b/w
//#include <GxGDEW029Z10/GxGDEW029Z10.h>    // 2.9" b/w/r
//#include <GxGDEW027C44/GxGDEW027C44.h>    // 2.7" b/w/r
//#include <GxGDEW027W3/GxGDEW027W3.h>      // 2.7" b/wb
//#include <GxGDEW042T2/GxGDEW042T2.h>      // 4.2" b/w
//#include <GxGDEW042Z15/GxGDEW042Z15.h>    // 4.2" b/w/r
//#include <GxGDEW0583T7/GxGDEW0583T7.h>    // 5.83" b/w
#include <GxGDEW075T8/GxGDEW075T8.h>      // 7.5" b/w
//#include <GxGDEW075Z09/GxGDEW075Z09.h>    // 7.5" b/w/r


// Error code to return when we fail to initiate an HTTP GET request.
#define HTTP_NOT_INITIATED -999

/* Timestamps to see where we spend our waking milliseconds... */
#define TS_WAKE 0
#define TS_EINK_ON 1
#define TS_WIFI_CONNECTED 2
#define TS_LOADING_IMAGE 3
#define TS_IMAGE_LOADED 4
#define TS_IMAGE_DISPLAYED 5
#define TS_MAX 6
#define TIMESTAMP(A) timestamps[A] = millis()
#define TIMESTAMP_ARGS timestamps[TS_WAKE], timestamps[TS_EINK_ON], timestamps[TS_WIFI_CONNECTED], \
                       timestamps[TS_LOADING_IMAGE], timestamps[TS_IMAGE_LOADED], \
                       timestamps[TS_IMAGE_DISPLAYED]
#define TIMESTAMP_FMT "%dx%dx%dx%dx%dx%d"

/* Macros to format a MAC address into a nice string. */
#define CLIENT_ID_ARGS(A) A[0], A[1], A[2], A[3], A[4], A[5]
#define CLIENT_ID_FMT "%02x:%02x:%02x:%02x:%02x:%02x"


const char *image_url = "/get_image_timed?voltage=%d&type=%s&client_id="CLIENT_ID_FMT;

int timestamps[TS_MAX];
int voltage = 0;

GxIO_Class io(SPI, PA_CS, PA_DC, PA_RST);
GxEPD_Class display(io, PA_RST, PA_BUSY);


/*
 * Print an error message onto the eInk display using GxEPD.
 *
 * Parameters:
 *   msg IN: Message to display.
 */

void
error_message(const char *msg) {
   Serial.println(msg);
#ifdef DISPLAY_ON
   display.fillScreen(GxEPD_WHITE);
   display.setTextColor(GxEPD_BLACK);
   display.setFont(&FreeMonoBold12pt7b);
   display.setCursor(0,14);
   display.println(msg);
   display.update();
#endif
   delay(3000);
}


/**
 * Send an HTTP GET request to the server.
 *
 * Parameters:
 *   wifi IN: WiFi client instance to use to send request.
 *   http IN: HTTP client instance to use.
 *   url IN: URL to request HTTP data from.
 *
 * Result:
 *   Returns 0 if the request was successfully submitted, HTTP error
 *   codes if an HTTP error was encountered or HTTP_NOT_INITIATED if
 *   an error was encontered before the HTTP request could be sent.
 */

int
get_http_data(WiFiClient &wifi, HTTPClient &http, const char *url)
{
  int result;
  char buf[256];

  Serial.printf("Attempting to get http://%s%s\n", HOST, url);

  wifi.setTimeout(HTTP_TIMEOUT_MS);
  http.setTimeout(HTTP_TIMEOUT_MS);

  if (!http.begin(wifi, HOST, HOST_PORT, url)) {
    error_message("HTTP: Unable to connect.");
    return HTTP_NOT_INITIATED;
  }

  /* If we get here we are connected. Let's pull data... */
  result = http.GET();

  if (result > 0) {
      /* Apparently something worked... */
      Serial.printf("HTTP Get returned: %d\n", result);

      if (result == HTTP_CODE_OK || result == HTTP_CODE_MOVED_PERMANENTLY) {
        result = 0;
      } else {
        sprintf(buf, "HTTP returned unhappiness code %d", result);
        error_message(buf);
      }
  } else {
    /* We got an HTTP error. */
    sprintf(buf, "HTTP Server returned error %d (%s)\n",
            result, http.errorToString(result).c_str());
    error_message(buf);
  }

  return result;
}


/**
 * Read the HTTP GET reply off the wire. The first four bytes are the delay
 * until the next image is requested (in seconds), in network byte order.
 *
 * The remaining data gets streamed directly into the GxEPD image buffer, since
 * we don't have enough RAM available on the 8266 to make another copy of the
 * data.
 *
 * Parameters:
 *    stream IN: Stream to read the data from.
 *    sleep_time OUT: Time (in seconds) to sleep until the next request is due.
 *      Already takes the amount of time into account that it takes to update the
 *      eInk display.
 */

void
display_image(Stream *stream, unsigned int &sleep_time)
{
  unsigned int start = millis();
  unsigned int delta;

  display.streamBitmap(stream);
  TIMESTAMP(TS_IMAGE_LOADED);

  // If deep sleep is active, put WiFi to sleep while updating the eInk display.
  // WiFi is our largest battery drain, so shut it down ASAP.
#ifdef USE_DEEP_SLEEP
  WiFi.forceSleepBegin();
  delay(1);
#endif

#ifdef DISPLAY_ON
  display.update();
#else
  delay(500);
#endif

  TIMESTAMP(TS_IMAGE_DISPLAYED);
  delta = millis() - start;

  if (delta >= sleep_time) {
    sleep_time = 1;
  } else {
    sleep_time -= delta;
  }

  // Prevent server-side bugs from sending us into deep sleep forever.
  // Also, the 8266 cannot stay in deep sleep for more than about an hour.
  if (sleep_time > MAX_SLEEP_TIME) {
     sleep_time = MAX_SLEEP_TIME;
  }
}


/**
 * Sleep for the specified amount of milliseconds. Then restart the ESP8266.
 *
 * Parameters:
 *   retry_ms IN: Amount of milliseconds to sleep.
 *
 * Note:
 *   This function never returns.
 */

void
sketch_done(unsigned int retry_ms)
{
   Serial.printf("Going to sleep for %d milliseconds. Timestamps:"TIMESTAMP_FMT "\n", retry_ms, TIMESTAMP_ARGS);
#ifdef USE_DEEP_SLEEP
   WiFi.disconnect(true);
   yield();
   WiFi.mode(WIFI_OFF);
   Serial.flush();
   // Sleeping with WAKE_RF_DISABLED means no WiFi for the ~350ms that
   // the boot loader takes.  When we wake up from deep sleep, the 8266
   // will have reset and begin executing "setup" again.
   ESP.deepSleep(retry_ms*1000, WAKE_RF_DISABLED);
#else
   delay(retry_ms);
   ESP.restart();
#endif

   // We should never get here.
}


/**
 * Connect to wifi or sleep.
 *
 * This function attempts to establish a WiFi connection. Failing to do so, the ESP8266 will
 * sleep for NO_WIFI_SLEEP milliseconds before waking up again and restarting/waking from
 * deep sleep.
 */

void
wifi_connect()
{
   wl_status_t wresult;

#ifdef NO_DHCP
   IPAddress ip(MY_IP_ADDRESS);
   IPAddress gateway(MY_GATEWAY);
   IPAddress subnet(MY_NETMASK);

   WiFiConnector conn(WIFI_SSID, WIFI_PASSWORD, ip, gateway, subnet, RTC_WIFI_OFFSET);
#else
   WiFiConnecotr conn(WIFI_SSID, WIFI_PASSWORD, RTC_WIFI_OFFSET);
#endif

   Serial.println("Attempting WiFi connection.");
   conn.startConnecting();
   wresult = conn.waitForConnection();

   if (wresult != WL_CONNECTED) {
      Serial.println("No WiFi.");
      sketch_done(NO_WIFI_SLEEP);
   }
}


/*
 * Arduino setup procedure.
 */

void
setup() {
   // Turn on WiFi, set SPI speed (Sparkfun recommends this for the Thing)
   // and init the eInk display driver.
   TIMESTAMP(TS_WAKE);
   WiFi.mode( WIFI_OFF );
   WiFi.forceSleepBegin();
   yield();
   SPI.setFrequency(1000000);

#ifdef DISPLAY_ON
   display.init(115200);
#else
  Serial.begin(115200);
#endif

  Serial.println("Hello there!");
  voltage = analogRead(A0);
  Serial.printf("Voltage reading: %d\n", voltage);
  TIMESTAMP(TS_EINK_ON);

  wifi_connect();
  TIMESTAMP(TS_WIFI_CONNECTED);

  Serial.println("");
  Serial.print("Connected with IP:");
  Serial.print(WiFi.localIP());
  Serial.print("\n");
}


/*
 * Arduino loop procedure.
 */

void
loop() {
  WiFiClient client;
  HTTPClient http;
  int result;
  int size;
  uint8_t mac_address[6];
  char url_buffer[256];
  unsigned int sleep_time = 60000;

  WiFi.macAddress(mac_address);

  sprintf(url_buffer, image_url, (int)(VOLTAGE_CORRECTION*voltage), BATTERY_TYPE, CLIENT_ID_ARGS(mac_address));
  TIMESTAMP(TS_LOADING_IMAGE);

  result = get_http_data(client, http, url_buffer);
  if (result == 0) {
      Stream *stream = http.getStreamPtr();

      if (stream->readBytes((char*)&sleep_time, sizeof(sleep_time)) != sizeof(sleep_time)) {
        Serial.printf("Could not read sleep time.\n");
        result = 501;
      } else {
        sleep_time = 1000*ntohl(sleep_time);
        display_image(stream, sleep_time);
      }
  }

  if (result != HTTP_NOT_INITIATED) {
    http.end();
  }

  sketch_done(sleep_time);
}
