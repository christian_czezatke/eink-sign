/*
 * A class for smart WiFi connects. We cache the BSSID and channel using
 * RTC memory. This can significantly reduce the amount of time it takes
 * to cut down on WiFi connect times.
 */

#ifndef __SMART_CONNECT_H__
#define __SMART_CONNECT_H__

#include <lwip/def.h>
#include <ESP8266WiFi.h>

/* Magic number to indicate a valid entry in RTC memory. */
#define RTC_PERSISTENT_MAGIC 0xdeadaffe

/* Default WiFi connect timeouts when doing a fast connect (with cached BSSID/channel)
 * vs. a slow connect. (In milliseconds)
 */
#define FAST_CONNECT_TIMEOUT 3000
#define SLOW_CONNECT_TIMEOUT 5000

class WiFiConnector
{
 public:
   /**
    * When instantiating a WiFiConnector, using the second constructor will avoid using
    * DHCP, for the fastest possible connect time.
    *
    * rtcOffset is the start offset within the RTC memory to persist WiFi data. Change this
    * to avoid conflicts with other data you store in RTC memory. -- We store a WiFi_Persistent
    * structure, so we use sizeof(WiFi_Persistent) bytes...
    *
    * ssid and passphrase are NOT copied. Do not free them until after waitForConnection.
    */
   WiFiConnector(const char *ssid, const char *passphrase, uint32_t rtcOffset);
   WiFiConnector(const char *ssid, const char *passphrase, const IPAddress &my_ip,
                 const IPAddress &my_gw, const IPAddress &my_netmask, uint32_t rtcOffset);

   wl_status_t startConnecting();
   wl_status_t waitForConnection(int32_t timeout_ms=0);

private:
   typedef struct {
      uint8_t bssid[WL_MAC_ADDR_LENGTH];
      uint16_t channel;
      uint32_t magic;
   } WiFi_Persistent;

   void beginDHCPSetup();
   void beginSetup(const IPAddress &my_ip, const IPAddress &my_gw, const IPAddress &my_netmask);

   wl_status_t connectFast();
   wl_status_t connectSlow();
   wl_status_t waitConnected(int32_t timeout_ms);

   void saveRTC();

   const char *ssid;
   const char *passphrase;
   bool connectingFast;
   uint32_t rtcOfs;
};

#endif
