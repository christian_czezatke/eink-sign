Documentation for the Code
**************************

The eInkDraw Module
-------------------

.. automodule:: einkdraw
   :members:

eInkDraw Layout Classes
-----------------------

This module contains classes that deal with the relative layout of
drawable elements to each other. Drawable elements can be laid out
in several forms of rows, or columns, they can be stacked on top of
each other or they can be embedded in others (like in a Frame).

These layouts can also be nested to create more complex arrangements.

.. automodule:: einkdraw.layout
   :members:

eInkDraw Glyph Classes
----------------------

Glyphs are simple graphic primitives that display text or graphics.

.. automodule:: einkdraw.glyphs
   :members:

eInkDraw User Interface Elements
--------------------------------

More complex user interface elements like windows, date/time related
elements and various forms of charts.

.. automodule:: einkdraw.uielements
   :members:

eInkDraw Bitmap Fonts
---------------------

Using vectorized fonts for small text on low-resolution monochrome displays gives
poor results. Therefore einkdraw integrates bitmapped fonts from the Adafruit
GFX Library.

The only class you need to worry about as a user of this library is the
FontManager and the get_font method.

.. automodule:: einkdraw.fonts
   :members:

eInkDraw Logging
----------------

Set the logging object to be used by the library.

.. automodule:: einkdraw.logger
   :members:
      
eInkDraw Constants
------------------

Module-wide constants used in eInkDraw.

.. automodule:: einkdraw.constants
   :members:
