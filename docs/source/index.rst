.. Eink Panel documentation master file, created by
   sphinx-quickstart on Sun Dec  9 23:13:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to eInkDraw's documentation!
======================================

Overview
--------

The characteristics of Electronic Ink (eInk) displays make them ideal
for applications where information is only updated relatively
infrequent and where one has to deal with a limited power budget.

This also implies that these displays will often be combined with low
power embedded processors, such as the ESP8266 that have limited
processing power.

Limited processing power implies that complex inages cannot be
rendered on these controllers. -- In order to avoid the restrictions
imposed by these low power embedded processors, it is possible to
render the image on the server side, and then transfer it to the
embedded processor for display.

eInkDraw is a rendering library that can provide these server side
rendering capabilities. It is based on PyCairo, the Python bindings
for the Cairo Graphics library of the Gtk project.

eInkDraw facilitates the drawing of complex shapes using Cairo, while
taking into account the characteristics of eInk displays, such as the
requirement to render monochrome and often low-resolution images:

.. image:: sample.png

The main purpose of eInkDraw is to create status displays for eInk
displays, but other use cases are possible as well.


eInkDraw provides building blocks, such as Labels (Text), Icons,
various charting primitives and "Window Frames". It also allows for
grouping building blocks into Columns, Rows and Stacks (drawn from
background to foreground), and these groups can also be nested.

This way fairly complex layouts can be achieved without the need of
too much pixel counting.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

