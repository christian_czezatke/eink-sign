"""
.. module:: einkdraw.glyphs

.. Primitive objects that einkdraw knows how to draw.

"""

import cairo
from einkdraw.constants import DEFAULT_FACE, DEFAULT_BOLD_FACE
from einkdraw.layout import drawing_on, Drawable
import einkdraw.fonts as fonts

class Icon(Drawable):
    """ Draw an icon from a :class:`cairo.SurfaceObject`.

    Args:
       width (int): Width of the object.
    
       height (int): Height of the object.

       icon (:class:`cairo.SurfaceObject`): Image to draw.

    .. note::

       Currently this performs no clipping. If an image is passed
       in that is larger, it will overwrite objects outside the
       :class:`Icon`'s bounding box.
    """

    def __init__(self, width, height, icon=None):
        self.icon = icon
        self.width = width
        self.height = height

        
    def set_value(self, icon):
        """ Set the image to draw.

        Args:
           icon (:class:`cairo.SurfaceObject`): Image to draw.        
        """
        self.icon = icon

    def draw(self, ctx):
        if self.icon is not None:
            with drawing_on(ctx):
                ctx.set_source_surface(self.icon)
                ctx.paint()

    def get_bbox(self):
        return self.width, self.height



class Label(Drawable):
    """A Label is a glyph that can show a line of text.

    Args:
    
       text (str): 
          Inital text to display. Can later be overriden with a call to set_value.

       width (int):
          Width of the label's bounding box.

       height (int):
          Height of the label's bounding box.

       face (:class:`~cairo.FontFace`):
          The font used to draw the text in the glyph.

       alignment (one of "left", "right", "centered"):
          Alignment of the text within the :class:`~Label`'s bounding box.

       inverse (bool):
           By default the text is drawn black on white. If this argument
           is set to true, it is drawn white on black.

       text_color (any of :class:`~layout.Background`.PATTERN_XXX constants):
           Specifies the color to use to draw the text.

       clip_text (bool):
           If this is set to False, the font size of the text will be scaled
           such that it will fit within the bounding box.

       descenders (bool):
           If set to true, then text scaling will be done in a way such that
           the algorithm is aware of descenders that might be in the text. (Descenders
           are elements of letters in a font that extend below the base line). -- Setting
           this to True will ensure that when displaying something like like "Text" (with no
           descenders), followed by another value like "quality" (with descenders) will
           not cause the text's base line to jump around.

           In some cases a :class:`~Label` might only show text
           without descenders (for example when it is only displaying
           numeric values. In this case descenders an be set to False,
           to force the base line of the text to be aligned with the
           lower boundary of the :class:`~Label`.

       .. note::

           When using clip_text=False or when specifying desceners=False and then specifying
           a text that has descenders might cause text to extend beyond the :class:`~Label`'s
           bounding box.

    """
    
    def __init__(self, text, width=100, height=16, face=DEFAULT_FACE, alignment="centered",
                 inverse=False, text_color=None, clip_text=False, descenders=True):
        self.text = text
        self.width = width
        self.height = height
        self.face = face
        self.alignment = alignment
        self.inverse = inverse
        self.clip_text=clip_text
        self.descenders = descenders
        self.text_color = text_color

    def set_value(self, value):
        self.text = value

    def resize(self, width=None, height=None):
        if width is not None:
            self.width = width
        if height is not None:
            self.height = height

    def get_bbox(self):
        return self.width, self.height

    def draw(self, ctx):
        val = self.text

        with drawing_on(ctx):
            if self.inverse:
                ctx.set_source_rgb(0,0,0)
                ctx.rectangle(0,0,self.width, self.height)
                ctx.fill()
                ctx.stroke()

            bitmap_font = type(self.face) == fonts.GfxFont
                
            if bitmap_font:
                x, y, width, height, dx, dy = self.face.get_dimensions(val, self.descenders)
                y_offset = self.height-(y+height)
                xlate = ctx.translate
            else:
                ctx.set_font_face(self.face)
                font_size, y_offset = self._fit_text(ctx)
                ctx.set_font_size(font_size)
                x, y, width, height, dx, dy = ctx.text_extents(val)
                xlate = ctx.move_to
                            
            if self.alignment == "centered":
                xlate(self.width//2 - width//2 - x//2, round(y_offset))
            elif self.alignment == "left":
                xlate(0, y_offset)
            elif self.alignment == "right":
                xlate(self.width - width - x, y_offset)
            if not self.text_color:
                if self.inverse:
                    ctx.set_source_rgb(1,1,1)
                else:
                    ctx.set_source_rgb(0,0,0)
            else:
                ctx.set_source(self.text_color)

            if self.clip_text:
                ctx.rectangle(0,0,self.width,self.height)
                ctx.clip()

            if bitmap_font:
                self.face.draw_text(ctx, val)
            else:
                ctx.show_text(val)
            ctx.stroke()


    def _fit_text(self, ctx):
        val = self.text

        with drawing_on(ctx):
            font_size = 7
            found = False
            prev_size = -1
            prev_y_offset = -1
            while not found:
                ctx.set_font_size(font_size)
                _, y, w, max_height, _, _ = ctx.text_extents(val)
                if self.descenders:
                    _, y, _, max_height, _, _ = ctx.text_extents("IlWqjp")
                if (self.clip_text or w <= self.width) and \
                   max_height <= self.height:
                    # text fits. we can go larger
                    prev_size = font_size
                    prev_y_offset = y
                    font_size += 1
                else:
                    found = True
        return round(prev_size), round(-prev_y_offset)
