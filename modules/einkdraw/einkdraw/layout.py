"""
.. module:: einkdraw.layout

   Basic layout classes for drawing on eInk displays.
"""
import cairo
from einkdraw.logger import debug

class drawing_on:
    """A helper for saving and restoring a :class:`pycairo.Context` within
    a draw function of an einkdraw :class:`Drawable`.

    This makes sure that :class:`pycairo.Context` save and restore calls
    always match up.

    Example::

    >>> def draw(self, ctx):
    >>>    with drawing_on(ctx):
    >>>           my_drawing_code_manipulating_ctx(ctx)

    """
    
    def __init__(self, ctx):
        """
        Args:
        ctx (:class:`pycairo.Context`): Context to draw on.
        """
        self.ctx = ctx

    def __enter__(self):
        self.ctx.save()

    def __exit__(self, type, value, traceback):
        self.ctx.restore()


class Drawable(object):
    """
    Drawable is the base class for all graphical elements
    defined by einkdraw. This is a base class and not
    meaningful to instantiate.
    """
    
    def __init__(self):
        pass

    def get_bbox(self):
        """ Return the size of the bounding box for this drawable.

        Returns:
          (int, int) Width and height of the bounding box.
        """
        pass

    def draw(self, ctx):
        """ Draw the content of this object to a pycairo Drawing context.

        Args:
           self (pycairo.Context): The drawing context to draw on.
        """
        
        pass



class Background(Drawable):
    """A uniformally filled rectangle, generally used as a background
    underneath other :class:`Drawable` instances.

    Args:
       width (int): Width of object

       height (int): Height of object)

       pattern: Any of the Background.PATTERN_XXX constants.
    """
    def __init__(self, width=100, height=100, pattern=None,
                 draw_outline=False):
        self.width = width
        self.height = height
        self.pattern = pattern
        self.draw_outline = draw_outline

    
    @staticmethod
    def _get_grey_pattern1():
        img = cairo.ImageSurface(cairo.FORMAT_RGB24, 2, 2)
        ctx = cairo.Context(img)
        ctx.set_line_width(1)
        ctx.set_antialias(cairo.Antialias.NONE)
        ctx.set_source_rgb(1,1,1)

        ctx.rectangle(0,0,1,1)
        ctx.fill()
        ctx.rectangle(1,1,1,1)
        ctx.fill()
        del ctx

        grey_pattern = cairo.SurfacePattern(img)
        grey_pattern.set_extend(cairo.EXTEND_REPEAT)
        return grey_pattern


    @staticmethod
    def _get_grey_pattern2():
        img = cairo.ImageSurface(cairo.FORMAT_RGB24, 3, 3)
        ctx = cairo.Context(img)
        ctx.set_line_width(1)
        ctx.set_antialias(cairo.Antialias.NONE)
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(0,0,3,3)
        ctx.fill()
        ctx.set_source_rgb(0,0,0)
        ctx.rectangle(1,1,1,1)
        ctx.fill()
        del ctx

        grey_pattern = cairo.SurfacePattern(img)
        grey_pattern.set_extend(cairo.EXTEND_REPEAT)
        return grey_pattern


    @staticmethod
    def _get_grey_pattern3():
        img = cairo.ImageSurface(cairo.FORMAT_RGB24, 3, 3)
        ctx = cairo.Context(img)
        ctx.set_line_width(1)
        ctx.set_antialias(cairo.Antialias.NONE)
        ctx.set_source_rgb(0,0,0)
        ctx.rectangle(0,0,3,3)
        ctx.fill()
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(1,1,1,1)
        ctx.fill()
        del ctx

        grey_pattern = cairo.SurfacePattern(img)
        grey_pattern.set_extend(cairo.EXTEND_REPEAT)
        return grey_pattern


    @staticmethod
    def _get_black_pattern():
        img = cairo.ImageSurface(cairo.FORMAT_RGB24, 1, 1)
        ctx = cairo.Context(img)
        ctx.set_line_width(1)
        ctx.set_antialias(cairo.Antialias.NONE)
        ctx.set_source_rgb(0,0,0)
        ctx.rectangle(0,0,1,1)
        ctx.fill()
        del ctx

        grey_pattern = cairo.SurfacePattern(img)
        grey_pattern.set_extend(cairo.EXTEND_REPEAT)
        return grey_pattern


    @staticmethod
    def _get_white_pattern():
        img = cairo.ImageSurface(cairo.FORMAT_RGB24, 1, 1)
        ctx = cairo.Context(img)
        ctx.set_line_width(1)
        ctx.set_antialias(cairo.Antialias.NONE)
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(0,0,1,1)
        ctx.fill()
        del ctx

        grey_pattern = cairo.SurfacePattern(img)
        grey_pattern.set_extend(cairo.EXTEND_REPEAT)
        return grey_pattern


    PATTERN_WHITE = _get_white_pattern.__func__() #: A white background
    PATTERN_MEDIUM_GREY = _get_grey_pattern1.__func__() #: A 50% dithered background.
    PATTERN_LIGHT_GREY = _get_grey_pattern2.__func__() #: A lightly dithered grey background
    PATTERN_DARK_GREY = _get_grey_pattern3.__func__() #: A tightly dithered grey background.
    PATTERN_BLACK = _get_black_pattern.__func__() #: A black background
    PATTERN_TRANSPARENT = None #: A transparent background


    def draw(self, ctx):
        if self.pattern == Background.PATTERN_TRANSPARENT:
            return

        with drawing_on(ctx):
            ctx.set_line_width(0)
            ctx.rectangle(0,0,self.width,self.height)
            ctx.set_source(self.pattern)
            ctx.fill()
            ctx.stroke()
            if self.draw_outline:
                ctx.set_source_rgb(0,0,0)
                ctx.set_line_width(1)
                ctx.rectangle(0,0,self.width,self.height)
                ctx.stroke()


    def get_bbox(self):
        return self.width, self.height

    def resize(self, width=None, height=None):
        if width != None:
            self.width = width
        if height != None:
            self.height = height

    

class Wrapper(Drawable):
    """A base class to derive objects from that wrap :class:`Drawable`
    instances. This saves you from having to implement trival draw
    and get_bbox methods.

    Args:
      toplevel_object (:class:`Drawable`) Toplevel ekinkdraw drawable to wrap.
    """
    def __init__(self, toplevel_object):
        self.toplevel_object = toplevel_object

    def draw(self, ctx):
        self.toplevel_object.draw(ctx)

    def get_bbox(self):
        return self.toplevel_object.get_bbox()
    

class Spacer(Drawable):
    """A layout helper class that inserts space between drawables in
    containers derived from :class:`Linear` or :class:`FixedLinear`.

    Args:
       amount (int): Amount of space to insert both horizontally and vertically.
    """

    def __init__(self, amount=10):
        self.width = self.height = amount

    def get_bbox(self):
        return self.width, self.height

    def resize(self, width=None, height=None):
        """Changes the size of a :class:`spacer` after its creation.

        Args:
           width (int): If not None, set the horizontal size to the value specified.

           height (int): If not None, set the vertical size to the value specified.

        """

        if width is not None:
            self.width = width
        if height is not None:
            self.height = height


class Frame(Drawable):
    """
    Puts a frame around an embedded :class:`Drawable`. There is an outer
    (white) frame, a middle (black) and an inner(white) rectangle that
    the frame is comprised of.

    Args:
       drawable (:class:`Drawable`): Object to frame.
    
       outer (int): Width of the outer (white) rectangle of the frame.

       middle (int): Width of the middle (black) rectangle of the frame.

       inner (int): Width of the inner (white) rectangle of the fame.

    """
    def __init__(self, drawable, outer=1, middle=1, inner=1):
        self.drawable = drawable
        self.outer = outer
        self.middle = middle
        self.inner = inner


    def draw(self, ctx):
        i_w, i_h = self.drawable.get_bbox()

        with drawing_on(ctx):
            ctx.set_source_rgb(0,0,0)
            ctx.set_line_width(self.middle)
            ctx.rectangle(self.outer+self.middle/2, self.outer+self.middle/2,
                          i_w + 2*self.inner+self.middle/2,
                          i_h + 2*self.inner+self.middle/2)
            ctx.stroke()

        with drawing_on(ctx):
            ctx.translate(self.outer+self.middle+self.inner,
                          self.outer+self.middle+self.inner)
            self.drawable.draw(ctx)


    def get_bbox(self):
        w, h = self.drawable.get_bbox()
        return w + 2*(self.outer+self.middle+self.inner), \
               h + 2*(self.outer+self.middle+self.inner),


class Linear(Drawable):
    """Base class for all vertical or horizontal alignment classes
    that derive their overall size from the bounding box of the
    :class:`Drawables` that they contain.
    """

    def __init__(self, items=[]):
        self.items = []
        self.add(items)

    def add(self, obj):
        """Add a drawable or a list of drawables.

        Args:
           obj (:class:`Drawable` or list thereof): Object(s) to add.
           
        """        
        if type(obj) is list or type(obj) is tuple:
            self.items.extend(obj)
        else:
            self.items.append(obj)



class FixedLinear(Wrapper):
    """Base class for horizontal or vertical layouts of :class:`Drawable`
    instances where the size of the layout container is fixed, and
    the list of objects in it are resized accordingly, to fill the
    container either horizontally or vertically.

    Args:
       width (int): Width of object.

       height (int): Height of object.

       stack ([(int, :class:`Drawable`, Background)]): A list of tuples, where the first
          element is an integer specifying the number of proportional shares
          within the :class:`FixedLinear` that are taken up by the 
          :class:`Drawable` that is the second element of each tuple in this
          list. The third elemetn is an of the Background.PATTERN_XX constants,
          for backgrount patterns. (Note that None means transparent background).

       orientation (str): Either "row" or "column", specifying whether
          the elements should be lined up horizontally or vertically.

    """
    def __init__(self, width, height, stack, orientation="column"):
        assert(orientation == "column" or orientation == "row")

        self.width = width
        self.height = height
        self.proportions = [x[0] for x in stack]
        self.labels = [x[1] for x in stack]
        self.backgrounds = [x[2] for x in stack]
        self.column = orientation == "column"

        if self.column:
            self.divisable_space = self.height
            self.resize_func = lambda x, y: x.resize(width=self.width, height=y)
            self.group_class = Column
            self.background_func = lambda x, y: Background(self.width, x, y)
        else:
            self.divisable_space = self.width
            self.resize_func = lambda x, y: x.resize(width=y, height=self.height)
            self.group_class = Row
            self.background_func = lambda x, y: Background(x, self.height, y)

        super().__init__(self._build_root_object())


    def _get_sizing_unit(self):
        total_shares = sum(self.proportions)
        return self.divisable_space/total_shares



    def _build_root_object(self):
        unit = self._get_sizing_unit()
        debug("Height unit: {0}, backgrounds {1}".format(unit, len(self.backgrounds)))
        assert(len(self.backgrounds) > 0)

        object_list = []

        # First: Rejiggle the size of each object.
        for obj, shares in zip(self.labels, self.proportions):
            self.resize_func(obj, shares*unit)

        # build backgrounds -- merge backgrounds if you can
        backgrounds = []
        total_units = 0
        cur_bg = "invalid"
        for bg, shares in zip(self.backgrounds, self.proportions):
            if bg != cur_bg:
                # Finalize current background object
                if total_units > 0:
                    backgrounds.append(self.background_func(total_units*unit, cur_bg))
                cur_bg = bg
                total_units = shares
            else:
                total_units += shares

        if total_units > 0:
            backgrounds.append(self.background_func(total_units*unit, cur_bg))
            debug("Doing background {0} {1}".format(total_units, cur_bg))

        return Stack((self.group_class(backgrounds),
                      self.group_class(self.labels)))



class FixedColumn(FixedLinear):
    """A layout object that aligns elements vertically.

    See :class:`FixedLinear`

    """
    
    def __init__(self, width, height, stack):
        super().__init__(width, height, stack, "column")

        
class FixedRow(FixedLinear):
    """A layout object that aligns elements vertically.

    See :class:`FixedLinear`

    """
    
    def __init__(self, width, height, stack):
        super().__init__(width, height, stack, "row")

        

class Column(Linear):
    """A layout object that aligns :class:`Drawable` objects vertically.

    The width of the resulting :class:`Column` object is the maximum
    width of all its children.

    The height of the resulting :class:`Column` object is determined by the
    sum of the sizes of all the vertically stacked :class:`Drawable`
    instances.

    Args:
       items (:class:`Drawable` or [:class:`Drawable`]): List of child objects
          to arrange, listed from top to bottom.

    """    
    def __init__(self, items=[]):
        super().__init__(items)

        
    def draw(self, ctx):
        with drawing_on(ctx):
            for i in self.items:
                i.draw(ctx)
                _, h = i.get_bbox()
                ctx.translate(0, h)


    def get_bbox(self):
        w_total = 0
        h_total = 0

        for i in self.items:
            w, h = i.get_bbox()
            w_total = max(w_total, w)
            h_total += h

        return w_total, h_total


class Row(Linear):
    """A layout object that aligns :class:`Drawable` objects horizontally.

    The width of the resulting :class:`Column` object is determined by the
    sum of the widths of all the vertically stacked :class:`Drawable`
    instances.

    The height of the resulting :class:`Column` object is the maximum
    height of all its children.

    Args:
       items (:class:`Drawable` or [:class:`Drawable`]): List of child objects
          to arrange, listed from left to right.

    """
    def __init__(self, items=[]):
        super().__init__(items)

        
    def draw(self, ctx):
        with drawing_on(ctx):
            for i in self.items:
                i.draw(ctx)
                w, _ = i.get_bbox()
                ctx.translate(w, 0)
            ctx.stroke()


    def get_bbox(self):
        w_total = 0
        h_total = 0

        for i in self.items:
            w, h = i.get_bbox()
            w_total += w
            h_total = max(h_total, h)

        return w_total, h_total

    
class Stack(Linear):
    """A layout object that aligns :class:`Drawable` objects from front
    to back, drawing them over each other.

    The width and height of a :class:`Stack` is the maximum of the
    widths and heights of its children.

    Args:
       items (:class:`Drawable` or [:class:`Drawable`]): List of child objects
          to arrange, listed from back to front.

    """
    def __init__(self, items=[]):
        super().__init__(items)

        
    def draw(self, ctx):
        for i in self.items:
            i.draw(ctx)


    def get_bbox(self):
        width = height = 0
        for i in self.items:
            w, h = i.get_bbox()
            width = max(width, w)
            height = max(height, h)

        return width, height

    def resize(self, width=None, height=None):
        [x.resize(width, height) for x in items]




    
def dummy():
    """
    Args:
       name (str):  The name to use.

    Kwxxxargs:
       state (bool): Current state to be in.

    Returns:
       int.  The return code::

          0 -- Success!
          1 -- No good.
          2 -- Try again.

    Raises:
       AttributeError, KeyError

    A really great idea.  A way you might use me is

    >>> print public_fn_with_googley_docstring(name='foo', state=None)
    0

    BTW, this always returns 0.  **NEVER** use with :class:`MyPublicClass`.
    """
    pass
