#!/usr/bin/python3
"""
.. module:: einkdraw.fonts

..   A module to allow the usage of bitmapped Adafruit fonts with einkdraw.

   While einkdraw supports scalable fonts via pycairo, bitmapped fonts yield better
   results for small font sizes on monochrome low resolution displays, such as
   many eInk papers.

   This module is not very efficient on startup and uses a simple approach to parse
   the Adafruit font headers in pure-python, without the use of any C code or the
   C compiler.

   TODO: Wire this up to cffi.
"""

import os
import glob
import re
import cairo
import sys
from einkdraw.screen import Screen
from einkdraw.glyphs import drawing_on
from einkdraw.logger import error, warn, info, debug

# Adafruit fonts support three font families and a few one-off, small fonts.
font_groups = ["Mono", "Sans", "Serif"]

package_directory = os.path.dirname(os.path.abspath(__file__))


class Glyph(object):
    """
    A Glyph represents a symbol in a font.
    """

    def __init__(self, parent, defs):
        self.font = parent
        self.symbol = None
        self.bitmap_offset, self.width, self.height, self.x_advance, self.x_offset, self.y_offset = defs

        # How high does this character go above the writing line
        self.ascend = -(self.y_offset -1) if self.y_offset < 0 else 0

        # How low does this character go below the writing line
        self.descend = self.y_offset + self.height

        # We rendere characters on-demand, so by default we don't have a cairo surface
        # representation.
        self.surface = None


    def _get_pixel(self, x, y):
        """
        Check if the pixel is supposed to be set, according to the bitmap that describes
        this font.
        """

        linear_offset = y * self.width + x
        bit_offset = linear_offset % 8
        return bool(self.font.bitmaps[self.bitmap_offset+linear_offset//8] & (1 << (7-bit_offset)))


    def _put_pixel(self, ctx, x, y):
        """
        Draw the pixel onto a cairo ImageSurface.
        """

        ctx.rectangle(x, y, 1, 1)
        ctx.fill()


    def _render(self):
        """
        Create a cairo surface representing the Glyph. Noe that this will only be called
        on the first invocation of get_img, as we cache the rendered Glyph.
        """

        self.surface = cairo.ImageSurface(cairo.FORMAT_A1, self.width, self.height)
        ctx = cairo.Context(self.surface)

        ctx.set_line_width(0)

        for y in range(self.height):
            for x in range(self.width):
                if self._get_pixel(x, y):
                    self._put_pixel(ctx, x, y)
        ctx.stroke()
        del ctx


    def get_img(self):
        """
        Return a cairo.Imagesruface (FORMAT_A1) representing the Glyph.
        """

        if self.surface is None:
            self._render()
        return self.surface


class GfxFont(object):
    """
    This class represents an Adafruit bitmap font. A very naive code parses the corresponding
    Adafruit font header and initializes the font class.
    """

    def __init__(self, file_name):
        """
        Initialize an GfxFont class from the Adafruit font header specified by file_name.
        """

        debug("GfxFont {0}".format(file_name))
        self.file_name = file_name
        self.group = None    # Mono/Sans/Serif or one-off font name.
        self.italic = False  # Is this an italic font?
        self.bold = False    # Is this a bold font?
        self.size = 0        # Nominal size of the font.
        self.bitmaps = None  # The bitmap array as read from the Adafruit font header.
        self.glyphs = None   # List of glyphs for font.
        self.ascii = None    # Info about the font (start/end symbol and line_advance)
        self.max_ascend = 0  # Max. ascend of any character in the font.
        self.max_descend = 0 # Max. descend of any character in the font.

        self._classify_by_name(file_name)
        self._noodle_font()


    def _classify_by_name(self, file_name):
        """
        Deduce font group/bold/italc from the font's name. Also deal with the
        one-off fonts.
        """

        attrs = re.findall("[A-Z][a-z]*", file_name)
        if not "Free" in attrs:
            if "Org" in attrs:
                # Orgdot
                self.group = "Orgdot"
                self.italic = False
                self.bold = False
                self.size = 6
            elif "Picopixel" in attrs:
                self.group = "Picopixel"
                self.italic = False
                self.bold = False
                self.size = 6
            elif "Tiny" in attrs:
                self.group = "Tiny"
                self.italic = False
                self.bold = False
                self.size = 3
            elif "Tom" in attrs:
                self.group = "TomThumb"
                self.italic = False
                self.bold = False
                self.size = 5
        else:
            self._classify_family(file_name, attrs)


    def _classify_family(self, file_name, attrs):
        """
        Font classification for Adafruit fonts in the Mono/Sans/Serif family.
        """

        attr_set = set(attrs)
        self.group = set(font_groups).intersection(attr_set).pop()
        self.italic = bool(set(("Italic", "Oblique")).intersection(attr_set))
        self.bold = "Bold" in attr_set
        match_obj = re.match("[^0-9]*([0-9]*)pt.*", file_name)
        self.size = int(match_obj.group(1))


    def _read_bitmaps(self, lines):
        res = bytearray()
        done = False
        while not done:
            l = lines[0]
            del lines[0]
            l = l.strip()

            l = re.sub("/\*.*\*/","", l)

            if l.find("#") == 0:
                # Line with a preprocessor directive, skip..
                continue
            done = l.find("}") != -1
            [res.append(int(x[1:], 16)) for x in re.findall("x[0-9a-fA-F]*", l)]
        return res


    def _read_glyphs(self, lines):
        res = []
        done = False
        while not done:
            l = lines[0]
            del lines[0]
            if l.find("#") == 0:
                # Line with a preprocessor directive, skip..
                continue

            done = "};" in l

            defs = [int(x) for x in l[l.find("{")+1:l.find("}")].replace(",", " ").split()]
            if len(defs) > 0:
                res.append(Glyph(self, defs))
        return res


    def _read_gfxfont(self, lines):
        res = dict()
        del lines[0]
        del lines[0]
        l = lines[0]
        del lines[0]
        numbers = l.replace(",", " ").replace("}", " ").replace(";", " ").split()
        res["start"] = int(numbers[0][2:], 16)
        res["end"] = int(numbers[1][2:], 16)
        res["line_advance"] = int(numbers[2])
        return res


    def _noodle_font(self):
        """
        Quick and dirty font header file parsing.
        """

        with open(self.file_name, "r") as f:
            lines = f.readlines()

        while lines:
            l = lines[0]
            del lines[0]
            if "Bitmaps[]" in l:
                self.bitmaps = self._read_bitmaps(lines)
            elif "Glyphs[]" in l:
                self.glyphs = self._read_glyphs(lines)
            elif "GFXfont" in l:
                self.ascii = self._read_gfxfont(lines)
            else:
                # All other lines we ignore
                pass

        self.max_ascend = max([x.ascend for x in self.glyphs])
        self.max_descend = max([x.descend for x in self.glyphs])


    def _get_glyph(self, c):
        """
        Return a Glyph instance for a given character.
        """

        start = self.ascii["start"]
        idx = ord(c)
        idx = min(max(start, idx), self.ascii["end"])
        return self.glyphs[idx-start]


    def get_dimensions(self, txt, normalize=False):
        """
        Get dimensions for a given text.

        Params:
           txt (str)
              Text for which to return dimensions.

           normalize (bool) 
               If False (default) the height, ascend and descend
               are calculated from the text specified ONLY. If True, height, ascend
               and descend are calculated using the maximum values for the font.

               This is helpful for font layout, since it prevents text from "jumping
               up and down" on labels should the text change ("foobar" vs. "quilt",
               for example).

        Result:
           Returns a typle equivaltent to pycairo's Context.text_extent.
           (Top left corner offset for text bounding box from current position,
           width, height of bounding box around text, x and y amount by which
           the current position should be advanced after drawing the text.
        """

        ascend = 0
        descend = 0
        width = 0
        x_offset = 0

        glyph_list = [self._get_glyph(x) for x in txt]
        if len(glyph_list) == 0:
            return 0, 0, 0, 0

        x_offset = glyph_list[0].x_offset
        for g in glyph_list:
            ascend = max(ascend, g.ascend)
            descend = max(descend, g.descend)
            width += g.x_advance

        if normalize:
            ascend = self.max_ascend
            descend = self.max_descend
        return x_offset, -ascend, width, ascend+descend, width, self.ascii["line_advance"]


    def draw_text(self, ctx, txt):
        """
        Render a text using a pycairo.Context

        Params:
           ctx (pycairo.Context)
              Context on which to render a text.
        """

        with drawing_on(ctx):
            for c in txt:
                g = self._get_glyph(c)
                img = g.get_img()
                ctx.translate(g.x_offset, g.y_offset)

                with drawing_on(ctx):
                    ctx.mask_surface(img)
                    ctx.fill()
                ctx.translate(g.x_advance-g.x_offset, -g.y_offset)



class FontManager(object):
    """
    A class through which GfxFont instances can be located.

    Given a family name, italic/bold settings and a max. size, the font
    manager finds the closest match for a bitmapped Adafruit font.
    """

    instance = None

    @staticmethod
    def get():
        """
        Obtain the font manager instance.
        """

        if FontManager.instance is None:
            FontManager.instance = FontManager()
        return FontManager.instance


    def __init__(self, font_dir=os.path.join(package_directory, "Fonts")):
        font_files = glob.glob(os.path.join(font_dir, "*.h"))
        fonts = [GfxFont(x) for x in font_files ]

        group_dict = {}
        for f in fonts:
            if f.group not in group_dict.keys():
                group_dict[f.group] = []
            group = group_dict[f.group]

            known_sizes = [x[0] for x in group]
            if f.size not in known_sizes:
                group.append([f.size, [f]])
            else:
                font_list = [x[1] for x in group if x[0] == f.size][0]
                font_list.append(f)

        # Now sort all the font groups by size:
        for g in group_dict.keys():
            group_dict[g] = sorted(group_dict[g], key=lambda x: x[0])

        self.font_groups = group_dict


    def _filter_group(self, group, bold, italic):
        res = []
        for size_list in group:
            new_entry = [x for x in size_list[1] if x.bold==bold and x.italic==italic]
            if len(new_entry) > 0:
                assert len(new_entry) == 1
                res.append(new_entry[0])
        return res


    def find_font(self, group="Mono", bold=False, italic=False, descend=True, max_size=99):
        """
        Find a font matching a description:

        Args:
           group (str)
              Either an Adafruit font group (Mono/Sans/Serif) or one of the small font
              names (Orgdot/Picopixel/Tiny/TomThumb).

           bold (bool)
              True if looking for the bold version of a font.

           italic (bool)
              True if looking for the italic version of a font.

           descend (bool)
              If True, take descenders (characters descending BELOW the writing
              line) into account when finding a matching font size.

           max_size (int)
              Maximum font size allowed.

        Result:
          Returns a GfxFont class instances or None, if no matching font could be found.
        """

        try:
            # Build a list of matching fonts. List will be sorted from small to large.
            fonts = self._filter_group(self.font_groups[group], bold, italic)

            # See if we have to take descends into account
            if descend:
                font = [x for x in fonts if x.max_ascend + x.max_descend <= max_size][-1]
            else:
                font = [x for x in fonts if x.max_ascend <= max_size][-1]
        except:
            warn("No font found.")
            font = None
        return font


def run():
    """
    Test/Sample code.
    """
    
    fm = FontManager()

    s = Screen.Waveshare75Mono()
    img = s.img
    ctx = cairo.Context(img)
    font_opt = cairo.FontOptions()
    font_opt.set_antialias(cairo.Antialias.NONE)
    font_opt.set_hint_metrics(cairo.HINT_METRICS_ON)
    font_opt.set_hint_style(cairo.HINT_STYLE_FULL)
    ctx.set_font_options(font_opt)
    ctx.set_antialias(cairo.Antialias.NONE)

    # Start by clearing the display -- set a white backround
    ctx.set_source_rgb(1,1,1)
    ctx.rectangle(0,0, s.width, s.height)
    ctx.fill()
    ctx.stroke()

    ctx.set_source_rgb(0,0,0)
    ctx.translate(10,50)

    txt ="The quick brown fox jumped over the lazy_dog."

    for x in ["Mono", "Serif", "Sans"]:
        font = fm.find_font(x, max_size=99)
        ctx.save()
        font.draw_text(ctx, txt)
#        width, height, x_offset, ascend = font.get_dimensions(txt)
#        print("{} {} {} {} {}".format(txt, width, height, x_offset, ascend))

        ctx.restore()
        ctx.translate(0, font.ascii["line_advance"])
        #img.write_to_png("font.png")


    for x in ["Mono", "Serif", "Sans"]:
        font = fm.find_font(x, max_size=99, bold=True)
        ctx.save()
        if font:
            font.draw_text(ctx, txt)
#            width, height, x_offset, ascend = font.get_dimensions(txt)
#            print("{} {} {} {} {} {}".format(txt, width, height, x_offset, ascend, font.size))

        ctx.restore()
        if font:
            ctx.translate(0, font.ascii["line_advance"])
        else:
            ctx.translate(0, 40)
    img.write_to_png("font.png")

    with open("binary.img", "wb") as f:
        f.write(s.get_data())

if __name__ == "__main__":
    run()
