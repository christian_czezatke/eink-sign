"""
.. module:: einkdraw.uielements

.. Higher level user interface elements.
"""

import math
import datetime
import cairo
from einkdraw.constants import DEFAULT_FACE, DEFAULT_BOLD_FACE
from einkdraw.layout import Wrapper, Column, Row, Background, FixedColumn, Spacer, Drawable, drawing_on
from einkdraw.glyphs import Label
import einkdraw.fonts as fonts
from einkdraw.logger import error, warn, info, debug


def linear_mapping(x, x_min, x_max, y_min, y_max):
    """
    A function performing a linear mappnig for a value x from the interval [x_min,x_max]
    to the interval [y_min,y_max].
    """

    # y = kx + d
    k = (y_max-y_min) / (x_max-x_min)
    d = y_min - k * x_min
    return k * x + d


class Window(Wrapper):
    """A window consists of a title bar, a frame around its content and
    an embedded :class:`Drawable` instance, representing the content of
    the window.

    Args:
        title (str):
           Name of window to appear in title bar.

        content (:class:`layout.Drawable`):
           Content of the window.

        title_height (int):
           Height of the title bar.

        frame_width (int):
           Width of the window border.

    """

    def __init__(self, title, content, title_height=19, frame_width=1):
        w, h = content.get_bbox()
        fw = frame_width
        th = title_height

        fm = fonts.FontManager.get()
        font = fm.find_font(group="Sans", bold=False, max_size=th)
        if font is None:
            error("Cannot get title bar font.")
            font = DEFAULT_BOLD_FACE
        
        title_bar = Column((Background(w+2*fw,1, Background.PATTERN_BLACK),
                            Label(title, w+2*fw, th-2*fw, inverse=True,
                                  face=font)))
        row_1 = Row((Background(fw, h, Background.PATTERN_BLACK),
                     content,
                     Background(fw, h, Background.PATTERN_BLACK)))
        column = Column((title_bar,row_1,
                         Background(w+2*fw, fw, Background.PATTERN_BLACK)))

        super().__init__(column)




class Calendar(Wrapper):
    """
    A Calendar widget displaying the current month, day and weekday.

    Args:
       width (int):
          Width of the widget.

       height (int):
          Height of the widget.

       auto_time (bool):
          If true, the current date is used to render the widget when its
          draw method is called.

       day_face (:class:`~cairo.FontFace`):
          Font to use to draw the current day of the month.

       minor_face (:class:`~cairo.FontFace`):
          Font to use to draw the month name and the weekday name.

       day_background (any of :class:`~layout.Background`.PATTERN_XXX):
          Background pattern to use to draw the background for the day of month
          part of the widget.

       minor_background (any of :class:`~layout.Background`.PATTERN_XXX):
          Background pattern to use to draw the background for the name of month
          and the weekday part of the widget.

       offset (int):
          Offset (in seconds) to add to the current time when auto_time
          is set to True. This can be used to compensate for rendering
          delays on slower platforms.
       """

    _weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                 "Saturday", "Sunday"]
    _months = [ "", "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"]


    def __init__(self, width, height, auto_time=True, day_face=DEFAULT_FACE,
                 minor_face=DEFAULT_FACE, day_background=None,
                 minor_background=None, offset=0):

        self.width = width
        self.height = height
        self.auto_time = auto_time
        self.offset = offset
        self.month_label = Label(text="undefined", face=minor_face)
        self.day_label = Label(text="?", face=day_face)
        self.weekday_label = Label(text="undefined", face=minor_face)

        self.day_background = day_background
        self.minor_background = minor_background

        self.day = 1
        self.month =1
        self.weekday = 0

        super().__init__(self._build_toplevel_object(day_background,
                                                     minor_background))


    def set_value(month=None, day=None, weekday=None):
        """
        Set the current day of month, month and weekday.

        Args:
           month (int):
             Number of the month. Values from 1 to 12 are valid.

           day (int):
             Day of the month. Values from 1 to 31 are valid. There is no cross-check
             whether the date is actually valid. (You could set a Feb. 31st, for example).

           weekday (int):
             Weekday Values from 0 to 6 are valid, with 0 being Monday, 6 being Sunday.

        .. note::
           If auto_time was set to true when instantiating a :class:`Calendar` object, then
           calling this function has no effect, since calling draw will cause any value
           specified to be overridden with the current date.
        """

        if month != None:
            assert(month> 0 and month <= 12)
            self.month = month

        if day != None:
            assert(day>=1 and day<=31)
            self.day = day

        if weekday != None:
            assert(day>=0 and day <=6)
            self.day = day


    def draw(self, ctx):
        if self.auto_time:
            self._get_time()

        with drawing_on(ctx):
            self.month_label.set_value(Calendar._months[self.month])
            self.day_label.set_value("{}".format(self.day))
            self.weekday_label.set_value(Calendar._weekdays[self.weekday])
            self.toplevel_object.draw(ctx)


    def _build_toplevel_object(self, day_background, minor_background):
        stack = FixedColumn(self.width, self.height,
                           ((3, Spacer(), minor_background),
                            (25, self.month_label, minor_background),
                            (2, Spacer(), minor_background),
                            (10, Spacer(), day_background),
                            (70, self.day_label, day_background),
                            (5, Spacer(), minor_background),
                            (25, self.weekday_label, minor_background),
                            (0, Spacer(), minor_background)))
        return stack

    def _get_time(self):
        now = datetime.datetime.now() + datetime.timedelta(seconds=self.offset)
        self.month = now.month
        self.day = now.day
        self.weekday = now.weekday()



class Clock(Drawable):
    """
    Draw an analog clock.

    Args:
       width (int):
          Width of the clock. Note that the clock is round, so height will
          be the same value.

       background (any of :class:`layout.Background`.PATTERN_XX):
          Pattern to use to draw the background of the clock's face. Note that
          None means transparent (PATTERN_TRANSPARENT).

       major_ticks (int):
          Size of the major tickmarks at the 12, 3, 6 and 9 o'clock position.

       minor_ticks (int):
          Size of all the other (minor) tickmarks.

       auto_time (bool):
          If true, the current time is always displayed. In this case set_value
          is basically ignored.

       offset (int):
          Offset (in seconds) to add to the current time when auto_time
          is set to True. This can be used to compensate for rendering
          delays on slower platforms.
    """

    def __init__(self, width, background=None, major_ticks=10, minor_ticks=6,
                 auto_time=True, offset=0):
        self.width = width
        self.background = background
        self.major_ticks = major_ticks
        self.minor_ticks = minor_ticks
        self.hour = 12
        self.minute = 0
        self.auto_time = auto_time
        self.offset = offset


    def set_value(self, hour, minute):
        """
        Unless auto_time was set to true, this adjusts the clock's value.

        Args:
           hour (int):
             Hour to display.

           minute (int):
             Minute to display.

        .. note::
           You can set the clock to values like "22:37" or even "54:87": The value
           is in any case converted to "minutes since midnight", and analog clocks
           imply a "modulo 12 hours" anyways.
        """

        self.hour = hour
        self.minute = minute


    def get_bbox(self):
        return self.width, self.width


    def draw(self, ctx):
        if self.auto_time:
            self._get_time()

        minutes = (self.hour % 12) * 60 + self.minute

        with drawing_on(ctx):
            ctx.translate(self.width/2, self.width/2)
            self._draw_face(ctx)
            self._draw_tickmarks(ctx)
            self._draw_hour_hand(ctx, minutes)
            self._draw_minute_hand(ctx, self.minute)
            ctx.stroke()


    def _get_time(self):
        now = datetime.datetime.now() + datetime.timedelta(seconds=self.offset)
        self.hour = now.hour
        self.minute = now.minute

    def _draw_face(self, ctx):
        if self.background:
            with drawing_on(ctx):
                ctx.set_source(self.background)
                ctx.arc(0,0, self.width/2-1, 0, 2*math.pi)
                ctx.fill()
                ctx.stroke()

        with drawing_on(ctx):
            ctx.set_source_rgb(0,0,0)
            ctx.arc(0,0, self.width/2-1, 0, 2*math.pi)
            ctx.stroke()

    def _draw_tickmarks(self, ctx):
        angle = 2*math.pi/12

        ctx.set_source_rgb(0,0,0)
        with drawing_on(ctx):
            for ticks in range(12):
                ctx.rotate(angle)
                ctx.move_to(0, -self.width/2+1)
                ctx.rel_line_to(0, self.minor_ticks)
                ctx.stroke()

        with drawing_on(ctx):
            for ticks in range(4):
                ctx.rotate(3*angle)
                ctx.move_to(0, -self.width/2+1)
                ctx.rel_line_to(0, self.major_ticks)
                ctx.stroke()


    def _draw_hour_hand(self, ctx, minutes):
        # How many minutes is a single 12 hour period (one lap for
        # the hour hand. -- Number of minutes in 12 hours.
        #
        # Like the minute hand, the hour hand is always drawn pointing
        # the 12 o'clock position and a drawing context rotation makes
        # the hand point to the correct location.
        one_lap = 60*12

        with drawing_on(ctx):
            ctx.rotate(minutes * 2*math.pi/one_lap)
            ctx.move_to(self.minor_ticks, self.minor_ticks)
            ctx.line_to(-self.minor_ticks, self.minor_ticks)
            ctx.line_to(0, -self.width/4-2)
            ctx.close_path()
            ctx.set_source(Background.PATTERN_BLACK)
            ctx.fill()
            ctx.stroke()


    def _draw_minute_hand(self, ctx, minutes):
        with drawing_on(ctx):
            ctx.rotate(minutes * 2*math.pi/60)
            ctx.move_to(self.minor_ticks, self.minor_ticks)
            ctx.line_to(-self.minor_ticks, self.minor_ticks)
            ctx.line_to(0, -self.width/2-2)
            ctx.close_path()
            ctx.set_source(Background.PATTERN_BLACK)
            ctx.fill()
            ctx.stroke()


class Barchart(Drawable):
    """
    This class draws a bar chart from a series of values.

    Bar charts can either be oriented horizontally, with bars growing
    from top to bottom, or vertically, with bars groing from left to
    right.

    Args:
       height (int):
         Height of the barchart if the chart is a horizontal bar chart.
         (In this case width should be set to None, since it is computed
         by len(categories) * bar_width).

       width (int):
         Width of the barchart if the chart is a vertical bar chart
         (In this case height should be set to None, since it is computed
         by len(categories) * bar_width).

       bar_width (int):
         Width of each bar.

       min_val (int):
         Minimum value for the base line of the bar chart (zero height bar).
         Smaller values will be clipped to this.

       max_val (int):
         Maximum value for the bar chart (full height bar). Larger values will
         be clipped to this.

       categories ([:class:`layout.Drawable`]):
         A list of :class:`Drawable` instances that can be resized. Each entry
         in the list represents one bar. A bar will be drawn in the right size by
         resizing the :class:`Drawable` corresponding to it, and drawing it.

       mapping (f(x, x_min, x_max, y_min, y_max)):
          A mapping function that maps values to display to drawable values.
          By default, a linear mapping function is used. Specifying other mapping
          functions would enable logarithmic charts, for example.

    .. note::
       The number of elements passed into "set_value" must match the number of
       categories specified when the object was constructed.

    """

    def __init__(self, height=None, width=None, bar_width=5,
                 min_val=0, max_val=100, categories=(),
                 mapping=linear_mapping):

        if height is None:
            self.value_dim = width
            self.x_orientation = "vertical"
        elif width is None:
            self.value_dim = height
            self.x_orientation = "horizontal"
        self.bar_width = bar_width
        self.categories = categories
        self.min_val = min_val
        self.max_val = max_val
        self.values = [min_val for x in categories]
        self.mapping = mapping


    def set_value(self, values):
        """
        Specify the values to display in the bar chart

        Args:
           values ([float]):
             List of numbers, one for each category.

        .. note:
           len(list) must be equal the the length of the categories list passed
           into the constructor (a value for each category must be specified).
        """

        assert len(values) == len(self.categories)
        self.values = [max(min(self.max_val, x), self.min_val) for x in values]

    def get_bbox(self):
        if self.x_orientation == "vertical":
            result = (self.value_dim, len(self.categories)*self.bar_width)
        else:
            result = (len(self.categories)*self.bar_width, self.value_dim)
        return result

    def draw(self, ctx):
        with drawing_on(ctx):
            for v, val in zip(self.categories, self.values):
                pixel_size = self._to_pixel(val)
                if self.x_orientation == "vertical":
                    width = pixel_size
                    height = self.bar_width
                    delta_x_next = 0
                    delta_y_next = self.bar_width
                    delta_x_this = 0 # no translation required for bars flush left.
                    delta_y_this = 0
                else:
                    width = self.bar_width
                    height = pixel_size
                    delta_x_next = self.bar_width
                    delta_y_next = 0
                    delta_x_this = 0
                    delta_y_this = self.value_dim - pixel_size

                v.resize(width, height)
                with drawing_on(ctx):
                    ctx.translate(delta_x_this, delta_y_this)
                    v.draw(ctx)
                ctx.translate(delta_x_next, delta_y_next)

    def _to_pixel(self, in_val):
        return self.mapping(in_val, self.min_val, self.max_val, 0, self.value_dim)


class Gauge(Drawable):
    """
    Draw a gauge-style element, to display a numeric value within a range.

    Gauges can either be circular, semi-circular or a setting in between.

    Args:

       width (int):
         Width of the gauge.

       height (int):
         Height of the gauge.

       chart_type (str):
         Any of "semicircular", "pie" or "circular".

       gauge_thickness (int):
         Thickness of the actual ring scale.

       min_val (float):
         Minimum value corresponding to the lower limit of the gauge.

       max_val (float):
         Maximum value corresponding to the upper limit of the gauge.

       face (:class:`cairo.FontFace`):
         Font to sue for outputting the current value of the gauge. None for
         default font.

       value_format (str):
         Python string "format" specifier, to turn the current gauge value into
         a printable string. -- For example to output a value as "XX.X", use
         python's float format string "2.1f".

       units (str):
         If set, identifies the units to display for the gauge value (ie: "mph",
         "mbar", etc... -- Units are displayed in slightly smaller font than
         the actual value.

       background (any of :class:`layout.Background`.PATTERN_XXX):
          Pattern to use to draw the background of the actual gauge. None
          is equivalent to transparent.

       foreground (any of :class:`layout.Background`.PATTERN_XXX):
          Pattern to use to draw the foreground of the gauge (ie: the graphical
          representation of the current value).

       mapping (f(x, x_min, x_max, y_min, y_max)):
          A mapping function that maps values to display to drawable values.
          By default, a linear mapping function is used. Specifying other mapping
          functions would enable logarithmic charts, for example.

    .. note::

       Only one of the "width" or the "height" properties should be set, since
       setting one also determines the other, because of the circular shape of
       a :class:`Gauge`.

    """

    def __init__(self, width=None, height=None, chart_type="pie", gauge_thickness=20,
                 min_val=0, max_val=100, mapping=linear_mapping,
                 face=None, value_format=None, units="",
                 background=None, foreground=Background.PATTERN_BLACK):
        self.chart_type = chart_type
        self.gauge_thickness = gauge_thickness
        if chart_type == "circular":
            self.angular_range = 2 * math.pi
        elif chart_type == "semicircular":
            self.angular_range = math.pi
        elif chart_type == "pie":
            self.angular_range = 1.5 * math.pi
        else:
            raise Exception("Invalid Gauge type: {0}".format(chart_type))

        self.min_val = min_val
        self.max_val = max_val
        self.width = width
        self.height = height
        self._recompute_properties()
        self.cur_val = min_val
        self.face = face
        self.value_format = value_format
        self.units = units
        self.background = background
        self.foreground = foreground
        self.mapping = mapping


    def set_value(self, x):
        self.cur_val = None
        if x is not None:
            self.cur_val = max(self.min_val, x)
            self.cur_val = min(self.max_val, self.cur_val)


    def get_bbox(self):
        return self.width, self.height


    def draw(self, ctx):
        with drawing_on(ctx):
            ctx.translate(self.r_o+1, self.r_o+1)
            self._draw_background(ctx)
            if self.cur_val is not None:
                self._draw_value(ctx)

        with drawing_on(ctx):
            self._draw_textval(ctx)


    def _recompute_properties(self):
        open_wedge = 2*math.pi - self.angular_range
        self.theta = open_wedge/2
        self.start_angle = math.pi/2 + self.theta

        if self.width is not None:
            # We have a given width. figure out how high things are going to get
            self.r_o = self.width/2.0 - 2
            self.r_i = self.r_o - self.gauge_thickness - 2
            self.height = (self.r_o+2) * (1+math.cos(self.theta))
        else:
            # We have a given height, figure out the width:
            self.r_o = self.height/(1+math.cos(self.theta)) - 2
            self.width = 2 * self.r_o + 2


    def _end_angle(self):
        start_angle = math.pi/2 + self.theta
        return self.mapping(self.cur_val, self.min_val, self.max_val,
                            start_angle, start_angle+self.angular_range)


    def _draw_semicircle(self, ctx, start_angle, end_angle, r_i, r_o, fill):
        sine = math.sin(start_angle)
        cosine = math.cos(start_angle)

        ctx.set_source_rgb(0,0,0)
        ctx.move_to(r_i*cosine, r_i*sine)
        ctx.line_to(r_o*cosine, r_o*sine)
        ctx.arc(0,0, r_o, start_angle, end_angle)

        sine = math.sin(end_angle)
        cosine = math.cos(end_angle)

        ctx.line_to(self.r_i*cosine, self.r_i*sine)
        ctx.arc_negative(0,0, r_i, end_angle, start_angle)
        ctx.close_path()
        if fill:
            ctx.set_source(fill)
            ctx.fill()
        ctx.stroke()


    def _draw_background(self, ctx):
        start_angle = math.pi/2 + self.theta
        end_angle = start_angle + self.angular_range
        self._draw_semicircle(ctx, start_angle, end_angle, self.r_i, self.r_o, self.background)
        self._draw_semicircle(ctx, start_angle, end_angle, self.r_i, self.r_o, None)

    def _draw_value(self, ctx):
        start_angle = math.pi/2 + self.theta
        end_angle = self._end_angle()
        self._draw_semicircle(ctx, start_angle, end_angle, self.r_i, self.r_o,
                              self.foreground)
        self._draw_semicircle(ctx, start_angle, end_angle, self.r_i, self.r_o, None)


    def _print(self, ctx, val, font_size):
        ctx.set_font_size(font_size)
        x1, y1, width1, height1, dx1, dy1 = ctx.text_extents(val)
        ctx.set_font_size(font_size/2)
        x2, y2, width2, height2, dx2, dy2 = ctx.text_extents(self.units)

        width = x1+dx1+width2
        height = height1

        ctx.set_source_rgb(0,0,0)

        ctx.set_font_size(font_size)
        if self.chart_type == "circular" or self.chart_type == "pie":
            ctx.move_to(self.width/2 - width/2-x1, self.r_o + height/2 + 1)
        else:
            ctx.move_to(self.width/2 - width/2-x1, self.height)
        ctx.show_text(val)

        if self.units:
            if self.chart_type == "circular" or self.chart_type == "pie":
                ctx.move_to(self.width/2 - width/2-x1 +dx1, self.r_o + height/2 + 1)
            else:
                ctx.move_to(self.width/2 - width/2-x1+dx1, self.height)
            ctx.set_font_size(font_size/2)
            ctx.show_text(self.units)


    def _draw_textval(self, ctx):
        if self.value_format is not None:
            if self.cur_val is not None:
                val = ("{0:"+self.value_format+"}").format(self.cur_val)
            else:
                val = "-?-"
            ctx.set_font_face(self.face)

            # now get a decent font size
            font_size = 16
            for font_size in range(16, 120):
                ctx.set_font_size(font_size)
                x1, y1, width1, height1, dx1, dy1 = ctx.text_extents(val)
                ctx.set_font_size(font_size/2)
                x2, y2, width2, height2, dx2, dy2 = ctx.text_extents(self.units)

                width = x1+dx1+width2
                height = height1

                if (width/2)**2 + (height/2)**2 > self.r_i**2 - 200:
                    font_size -= 1
                    break

            self._print(ctx, val, font_size)
            ctx.stroke()


class BatteryIndicator(Drawable):
    def __init__(self, unit_size=4, num_ticks=4,
                 foreground=Background.PATTERN_BLACK,
                 background=Background.PATTERN_WHITE,
                 bar=Background.PATTERN_MEDIUM_GREY):
        self._recompute_size(unit_size)
        self.percentage = 100
        self.num_ticks = num_ticks
        self.fg = foreground
        self.bg = background
        self.bar = bar
        self.unit_size = unit_size


    def get_bbox(self):
        return self.width, self.height


    def set_value(self, value):
        val = min(value, 100)
        val = max(val, 0)
        self.percentage = val


    def draw(self, ctx):
        with drawing_on(ctx):
            #First draw the outline of the battery
            ctx.set_source(self.bg)
            self._draw_battery_outline(ctx)
            ctx.fill()
            ctx.stroke()

            ctx.set_source(self.fg)
            ctx.set_line_width(2)
            self._draw_battery_outline(ctx)
            ctx.stroke()

            step = 100.0/self.num_ticks
            full = math.ceil(self.percentage/step)
            for x in range(self.num_ticks):
                self._draw_bar(ctx, x, self.fg if x < full else self.bar)


    def _recompute_size(self, unit_size):
        self.width = 9 * unit_size
        self.height = 4 * unit_size


    def _draw_battery_outline(self, ctx):
        ctx.move_to(0,0)
        ctx.line_to(8*self.unit_size+2, 0)
        ctx.line_to(8*self.unit_size+2, self.unit_size)
        ctx.line_to(9*self.unit_size, self.unit_size)
        ctx.line_to(9*self.unit_size, 3*self.unit_size)
        ctx.line_to(8*self.unit_size+2, 3*self.unit_size)
        ctx.line_to(8*self.unit_size+2, 4*self.unit_size)
        ctx.line_to(0, 4*self.unit_size)
        ctx.close_path()

    def _draw_bar(self, ctx, idx, color):
        with drawing_on(ctx):
            ctx.translate(idx*(8*self.unit_size/self.num_ticks), 0)
            ctx.set_source(color)
            ctx.rectangle(2, 2, 8*self.unit_size/self.num_ticks-2, 4*self.unit_size-4)
            ctx.fill()
            ctx.stroke()
