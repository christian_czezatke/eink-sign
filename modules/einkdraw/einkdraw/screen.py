"""
.. module: einkdraw.screen

.. Class for an eInk screen to draw on.
"""

import cairo
import time
from einkdraw.logger import info

class Screen(object):

    @staticmethod
    def Waveshare75Mono():
        return Screen(640, 384, "mono")
    
    def __init__(self, width, height, display_type):
        self.width = width
        self.height = height
        self.display_type = display_type
        self.img = cairo.ImageSurface(cairo.FORMAT_RGB24, width, height)
        self.content = None

        
    def set_content(self, content):
        self.content = content

        
    def draw(self):
        ctx = cairo.Context(self.img)
        if self.display_type == "mono" or self.display_type == "duo":
            # We are outputting to a monochromatic ePaper.
            # Turn off all antialiasing.
            font_opt = cairo.FontOptions()
            font_opt.set_antialias(cairo.Antialias.NONE)
            font_opt.set_hint_metrics(cairo.HINT_METRICS_ON)
            font_opt.set_hint_style(cairo.HINT_STYLE_FULL)
            ctx.set_font_options(font_opt)
            ctx.set_antialias(cairo.Antialias.NONE)

        # Start by clearing the display -- set a white backround
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(0,0, 640, 384)
        ctx.fill()
        ctx.stroke()

        if self.content is not None:
            self.content.draw(ctx)

        del ctx


    def write_to_png(self, file_name):
        self.img.write_to_png(file_name)        

    def get_data(self):

        mv = self.img.get_data()
        buf = mv.tobytes()
        
        result = bytearray([0x00]*(len(buf)//4//8))
        max_val = len(buf)/4
        cnt = 0
        ba_idx = 0

        start_time = time.time()
        while cnt < max_val:
            elem = 0
            for x in range(8):
                red = buf[4*cnt] # look at the red component
                if red < 128:
                    # White pixel. -- Turn on bit.
                    elem |= (1<<(7-x))
                cnt += 1
            result[ba_idx] = elem
            ba_idx += 1
        info("MONO conversion took {0}s".format(time.time()-start_time))
        del buf
        mv.release()
        return result

