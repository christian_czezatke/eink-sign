#!/usr/bin/python3
import os
import glob
import re
import cairo
import sys
from cffi import FFI
#from screen import Screen
#from glyphs import drawing_on

package_directory = os.path.dirname(os.path.abspath(__file__))

font_groups = ["Mono", "Sans", "Serif"]

ffibuilder = FFI()

class Glyph(object):
    def __init__(self, parent, defs):
        self.font = parent
        self.symbol = None
        self.bitmap_offset, self.width, self.height, self.x_advance, self.x_offset, self.y_offset = defs

        # How high does this character go above the writing line
        self.ascend = -(self.y_offset -1) if self.y_offset < 0 else 0

        # How low does this character go below the writing line
        self.descend = self.y_offset + self.height 

        self.surface = None

    def _get_pixel(self, x, y):
        linear_offset = y * self.width + x
        bit_offset = linear_offset % 8
        return bool(self.font.bitmaps[self.bitmap_offset+linear_offset//8] & (1 << (7-bit_offset)))

    def _put_pixel(self, ctx, x, y):
        #data = self.surface.get_data()

        #row_start = y*self.surface.get_stride()
        
        #ofs = 3*x + row_start
        #data[ofs] = 0 # data[ofs+1] = data[ofs+2] = 0

        ctx.rectangle(x, y, 1, 1)
        ctx.fill()
    
        
    def _render(self):
        self.surface = cairo.ImageSurface(cairo.FORMAT_A1, self.width, self.height)
        ctx = cairo.Context(self.surface)

#        ctx.set_antialias(cairo.Antialias.NONE)
#        ctx.set_source_rgba(1,1,1,1)
#        ctx.rectangle(0,0, self.width, self.height)
#        ctx.fill()
#        ctx.stroke()
        #del ctx
#        ctx.set_source_rgba(0,0,0,0)
        ctx.set_line_width(0)
        
#        ctx.set_line_width(1)
        for y in range(self.height):
            for x in range(self.width):
                if self._get_pixel(x, y):
#                    sys.stdout.write("*");
                    self._put_pixel(ctx, x, y)
                else:
#                    sys.stdout.write(" ")
                    pass
#            sys.stdout.write("\n")
        ctx.stroke()
        del ctx
        if os.path.exists("character.png"):
            os.unlink("character.png")
        #self.surface.write_to_png("character.png")
        #assert False

    def get_img(self):
        if self.surface is None:
            self._render()
        return self.surface
        
class GfxFont(object):
    def __init__(self, file_name):
        #print("GfxFont {0}".format(file_name))
        self.file_name = file_name
        self.group = None
        self.italic = False
        self.bold = False
        self.size = 0
        self.bitmaps = None
        self.glyphs = None
        self.ascii = None
        self.max_ascend = 0
        self.max_descend = 0

        self._classify_by_name(file_name)
        self._noodle_font()

        
    def _classify_by_name(self, file_name):
        attrs = re.findall("[A-Z][a-z]*", file_name)
        if not "Free" in attrs:
            if "Org" in attrs:
                # Orgdot
                self.group = "Orgdot"
                self.italic = False
                self.bold = False
                self.size = 6
            elif "Picopixel" in attrs:
                self.group = "Picopixel"
                self.italic = False
                self.bold = False
                self.size = 6
            elif "Tiny" in attrs:
                self.group = "Tiny"
                self.italic = False
                self.bold = False
                self.size = 3
            elif "Tom" in attrs:
                self.group = "TomThumb"
                self.italic = False
                self.bold = False
                self.size = 5
        else:
            self._classify_family(file_name, attrs)


    def _classify_family(self, file_name, attrs):
        attr_set = set(attrs)

        self.group = set(font_groups).intersection(attr_set).pop()
        self.italic = bool(set(("Italic", "Oblique")).intersection(attr_set))
        self.bold = "Bold" in attr_set
        match_obj = re.match("[^0-9]*([0-9]*)pt.*", file_name)
        self.size = int(match_obj.group(1))

    def _read_bitmaps(self, lines):
        res = bytearray()
        done = False
        while not done:
            l = lines[0]
            del lines[0]
            l = l.strip()

            l = re.sub("/\*.*\*/","", l)
            
            if l.find("#") == 0:
                # Line with a preprocessor directive, skip..
                continue
            done = l.find("}") != -1
            [res.append(int(x[1:], 16)) for x in re.findall("x[0-9a-fA-F]*", l)]
        return res
    

    def _read_glyphs(self, lines):
        res = []
        done = False
        while not done:
            l = lines[0]
            del lines[0]
            if l.find("#") == 0:
                # Line with a preprocessor directive, skip..
                continue

            done = "};" in l

            defs = [int(x) for x in l[l.find("{")+1:l.find("}")].replace(",", " ").split()]
            # XXX: Fix TomThom...
            if len(defs) > 0:
                res.append(Glyph(self, defs))
        return res

    def _read_gfxfont(self, lines):
        res = dict()
        del lines[0]
        del lines[0]
        l = lines[0]
        del lines[0]        
        numbers = l.replace(",", " ").replace("}", " ").replace(";", " ").split()
        res["start"] = int(numbers[0][2:], 16)
        res["end"] = int(numbers[1][2:], 16)
        res["line_advance"] = int(numbers[2])
        return res
        
    def _noodle_font(self):
        
        with open(self.file_name, "r") as f:
            lines = f.readlines()

        while lines:
            l = lines[0]
            del lines[0]
            if "Bitmaps[]" in l:
                self.bitmaps = self._read_bitmaps(lines)
            elif "Glyphs[]" in l:
                self.glyphs = self._read_glyphs(lines)
            elif "GFXfont" in l:
                self.ascii = self._read_gfxfont(lines)
            else:
                # All other lines we ignore
                #print("PASS {0}".format(l))
                pass

        self.max_ascend = max([x.ascend for x in self.glyphs])
        self.max_descend = max([x.descend for x in self.glyphs])


    def _get_glyph(self, c):
        start = self.ascii["start"]
        idx = ord(c)
        idx = min(max(start, idx), self.ascii["end"])

        return self.glyphs[idx-start]

    def get_dimensions(self, txt, normalize=False):
        ascend = 0
        descend = 0
        width = 0
        x_offset = 0

        glyph_list = [self._get_glyph(x) for x in txt]
        if len(glyph_list) == 0:
            return 0, 0, 0, 0

        x_offset = glyph_list[0].x_offset
        for g in glyph_list:
            ascend = max(ascend, g.ascend)
            descend = max(descend, g.descend)
            width += g.x_advance

        if normalize:
            ascend = self.max_ascend
            descend = self.max_descend
        #return width, ascend+descend, x_offset, ascend
        return x_offset, -ascend, width, ascend+descend, width, self.ascii["line_advance"]

    def draw_text(self, ctx, txt):
        with drawing_on(ctx):
            for c in txt:
                g = self._get_glyph(c)
                img = g.get_img()
                ctx.translate(g.x_offset, g.y_offset)

                with drawing_on(ctx):
                    #ctx.set_source_surface(img)
                    #ctx.mask_surface(img)
                
                    ctx.mask_surface(img)
                    ctx.fill()
                ctx.translate(g.x_advance-g.x_offset, -g.y_offset)

        
            
class FontManager(object):

    instance = None
    
    @staticmethod
    def get():
        if FontManager.instance is None:
            FontManager.instance = FontManager()
        return FontManager.instance
    
    
    def __init__(self, font_dir=os.path.join(package_directory, "Fonts")):
        font_files = glob.glob(os.path.join(font_dir, "*.h"))


        c_src = \
"""
#define PROGMEM
#include "gfxfont.h"
"""
        for x in font_files:
            stmt = "#include \"{0}\"\n".format(os.path.basename(x))
            c_src += stmt
        
        print(c_src)

        ffibuilder.set_source("_gfxfont", c_src,
                              include_dirs=[font_dir, package_directory])
        ffibuilder.compile(verbose=True)


        
        fonts = [GfxFont(x) for x in font_files ]

        group_dict = {}
        for f in fonts:
            if f.group not in group_dict.keys():
                group_dict[f.group] = []
            group = group_dict[f.group]

            known_sizes = [x[0] for x in group]
            if f.size not in known_sizes:
                group.append([f.size, [f]])
            else:
                font_list = [x[1] for x in group if x[0] == f.size][0]
                font_list.append(f)

        # Now sort all the font groups by size:
        for g in group_dict.keys():
            group_dict[g] = sorted(group_dict[g], key=lambda x: x[0])
            
        self.font_groups = group_dict


    def _filter_group(self, group, bold, italic):
        res = []
        for size_list in group:
            new_entry = [x for x in size_list[1] if x.bold==bold and x.italic==italic]
            if len(new_entry) > 0:
                assert len(new_entry) == 1
                res.append(new_entry[0])
        return res
        
    def find_font(self, group="Mono", bold=False, italic=False, descend=True, max_size=99):
        if True:
            # Now build a list of matching fonts. List will be sorted from small to large.
            fonts = self._filter_group(self.font_groups[group], bold, italic)
            #print("Filtered fonts: {}".format(fonts))
            #for f in fonts:
            #    print("{} {}".format(f.max_ascend, f.max_descend))
            
            # See if we have to take descends into account
            if descend:
                font = [x for x in fonts if x.max_ascend + x.max_descend <= max_size][-1]
            else:
                font = [x for x in fonts if x.max_ascend <= max_size][-1]
            
            print("Found font")
        else:
            print("Font not found")
            font = None
        return font
        

def run():
    
    fm = FontManager()
    # font = fm.find_font("Mono", max_size=30)
    # print("Found font size: {0} {1} {2}".format(font.file_name, font.size, font.max_ascend, font.max_descend))

    # txt = "Hello World!"

    # width, height, x_offset, ascend = font.get_dimensions(txt)
    # print("{} {} {} {} {}".format(txt, width, height, x_offset, ascend))


    s = Screen.Waveshare75Mono()
    img = s.img
    ctx = cairo.Context(img)
    font_opt = cairo.FontOptions()
    font_opt.set_antialias(cairo.Antialias.NONE)
    font_opt.set_hint_metrics(cairo.HINT_METRICS_ON)
    font_opt.set_hint_style(cairo.HINT_STYLE_FULL)
    ctx.set_font_options(font_opt)
    ctx.set_antialias(cairo.Antialias.NONE)

    # Start by clearing the display -- set a white backround
    ctx.set_source_rgb(1,1,1)
    ctx.rectangle(0,0, s.width, s.height)
    ctx.fill()
    ctx.stroke()

    ctx.set_source_rgb(0,0,0)
    ctx.translate(10,50)

    txt ="The quick brown fox jumped over the lazy_dog."

    for x in ["Mono", "Serif", "Sans"]:
        font = fm.find_font(x, max_size=99)
        ctx.save()
        font.draw_text(ctx, txt)
#        width, height, x_offset, ascend = font.get_dimensions(txt)
#        print("{} {} {} {} {}".format(txt, width, height, x_offset, ascend))
        
        ctx.restore()
        ctx.translate(0, font.ascii["line_advance"])
        #img.write_to_png("font.png")


    for x in ["Mono", "Serif", "Sans"]:
        font = fm.find_font(x, max_size=99, bold=True)
        ctx.save()
        if font:
            font.draw_text(ctx, txt)
#            width, height, x_offset, ascend = font.get_dimensions(txt)
#            print("{} {} {} {} {} {}".format(txt, width, height, x_offset, ascend, font.size))

        ctx.restore()
        if font:
            ctx.translate(0, font.ascii["line_advance"])        
        else:
            ctx.translate(0, 40)
    img.write_to_png("font.png")

    with open("binary.img", "wb") as f:
        f.write(s.get_data())

if __name__ == "__main__":
    run()
