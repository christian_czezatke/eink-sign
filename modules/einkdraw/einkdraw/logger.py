"""
.. module:: einkdraw.logger

..   Simple functions for logging.
"""

import logging
import logging.handlers

logger = None

def _get_default_logger():
    root = logging.getLogger()
    h = logging.handlers.RotatingFileHandler('einkserver.log', 'a', maxBytes=4000000,
                                             backupCount=10)
    f = logging.Formatter('%(asctime)s %(processName)-10s %(name)s %(levelname)-8s %(message)s')
    h.setFormatter(f)
    root.addHandler(h)
    return root


def _get_logger():
    global logger
    
    if logger is None:
        logger = _get_default_logger()
    return logger


def debug(txt):
    _get_logger().debug(txt)

    
def info(txt):
    _get_logger().info(txt)

    
def warn(txt):
    _get_logger().warn(txt)

    
def error(txt):
    _get_logger().error(txt)


def set_logger(log):
    """
    Set the logging object to be used by the library.

    It is recommended to call this function before invoking any other library functions,

    Params:
       log (logging.Logger)
          Logging object to use.
    """
    
    global logger

    logger = log
