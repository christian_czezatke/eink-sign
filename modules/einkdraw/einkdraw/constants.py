import cairo

# Liberation Sans looks good

#Arial
DEFAULT_FACE= cairo.ToyFontFace("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
#: Default font to use in einkdraw.

DEFAULT_BOLD_FACE= cairo.ToyFontFace("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
#: Default bold font to use in einkdraw.

SANS_FACE = cairo.ToyFontFace("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
SANS_BOLD_FACE = cairo.ToyFontFace("Arial", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
