#!/usr/bin/env python3

from setuptools import setup

setup(name='einkdraw',
      version='0.1',
      description='Server side drawing library for eInk-like displays.',
      url='https://bitbucket.org/christian_czezatke/einkdraw/src/master/modules/einkdraw',
      author='Christian Czezatke',
      author_email='einkdraw@ceci.at',
      license='BSD',
      packages=['einkdraw'],
      install_requires = [ 'pycairo' ],
      zip_safe=False,
      include_package_data=True)
